package com.hzdl.yxzc.util;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

public class IMGUtils {
    public String imgUpdate(HttpServletRequest req) throws FileUploadException, IOException {
        byte[] b=new byte[1024];
        //存储图片路径
        String folderpath="C:\\Users\\Administrator\\Desktop\\automobile\\YXZCProject\\src\\main\\webapp\\image\\logo";
        DiskFileItemFactory factory=new DiskFileItemFactory();
        ServletFileUpload upload=new ServletFileUpload(factory);
        upload.setHeaderEncoding("utf-8");
        List<FileItem> fileItems=upload.parseRequest(req);
        FileItem namefileItem = fileItems.get(0);
        FileItem imgfileItem = fileItems.get(1);
        System.out.println(namefileItem.getString("utf-8"));
        //获取图片上传之前的名字
            String imgName=namefileItem.getString("utf-8");
            String imgNameurl=URLEncoder.encode(imgName);
            imgNameurl=imgNameurl.replace("%","-");
            //获取文件的输入流输出流
            InputStream is=imgfileItem.getInputStream();
            FileOutputStream fos=new FileOutputStream(new File(folderpath+"/"+imgNameurl+".jpg"));
            //写入数据
            int len=0;
            while ((len=(is.read(b)))>-1){
                fos.write(b,0,len);
            }

            fos.close();
            is.close();
            System.out.println("图片上传成功:"+folderpath+"/"+imgName);
            return imgName;
        }
}
