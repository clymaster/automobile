package com.hzdl.yxzc.service.Impl;

import com.hzdl.yxzc.dao.EmpDao;
import com.hzdl.yxzc.dao.Impl.EmpDaoImpl;
import com.hzdl.yxzc.domain.Emp;
import com.hzdl.yxzc.domain.PageInfo;
import com.hzdl.yxzc.service.EmpService;

import java.util.List;

/**
 * 员工模块
 */
public class EmpServiceImpl implements EmpService {
    EmpDao empDao = new EmpDaoImpl();
    @Override
    public boolean save(Emp emp) {
        //判断一下是否注册过
        boolean reg = empDao.queryByUsername(emp);
        if (reg){
            //注册过
            return false;
        }
        //没注册过,调用save方法将信息存到数据库中
        boolean flag = empDao.save(emp);
        return flag;

    }

    @Override
    public PageInfo queryByName(String name, String currentpage, String rows) {
        List<Emp> list = empDao.queryByName(name,currentpage,rows);
        //总条数
        int datacount = empDao.queryCount(name);
        //总页数
        int pagetatol = datacount % Integer.parseInt(rows)==0?datacount/Integer.parseInt(rows):datacount/Integer.parseInt(rows)+1;

        PageInfo pageInfo = new PageInfo();
        pageInfo.setDatacount(datacount);
        pageInfo.setPagetatol(pagetatol);

        // List<EmpR> list_r = new ArrayList<>();

//        for (Emp emp : list) {
//            int storeId = emp.getStoreId();
//            if (storeId==1){
//
//            }
//        }

        pageInfo.setBrands(list);

        return pageInfo;
    }

    @Override
    public void delectById(String id) {

        empDao.delectById(id);
    }

    @Override
    public List<Emp> queryById(String id) {
        List<Emp> list = empDao.queryById(id);
        return list;
    }

    @Override
    public boolean modify(Emp emp) {
        boolean flag = empDao.modify(emp);
        return flag;
    }

    @Override
    public String findEmpNameByIdService(int id) {
        // 通过id字段查找员工表empName字段功能
        String empName = empDao.findEmpNameByIdService(id);
        return empName;
    }

    @Override
    public List findAllUserPas() {
        List list = empDao.findAllUserPas();
        return list;
    }

//    @Override
//    public String findEmpName(String id) {
//        String empName = empDao.findEmpName(id);
//        return empName;
//    }
}
