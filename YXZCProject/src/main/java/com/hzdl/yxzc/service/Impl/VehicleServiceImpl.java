package com.hzdl.yxzc.service.Impl;

import com.hzdl.yxzc.dao.Impl.VehicleDaoImpl;
import com.hzdl.yxzc.dao.VehicleDao;
import com.hzdl.yxzc.domain.*;
import com.hzdl.yxzc.service.VehicleService;

import java.util.ArrayList;
import java.util.List;

/**
 * 车辆模块
 */
public class VehicleServiceImpl implements VehicleService {
    VehicleDao vehicleDao = new VehicleDaoImpl();

    //品牌添加
    @Override
    public boolean addBrandService(String brandname) {
        List<Brand> brandDaoByName = vehicleDao.findBrandByNameDao(brandname);
        if (brandDaoByName == null) {
            return true;
        }
        return vehicleDao.addBrandDao(brandname) == 1 ? true : false;
    }

    //检索所有品牌
    @Override
    public PageInfo findAllBrandService(int currentpage, int pagesize) {
        List<Brand> brands = vehicleDao.findAllBrandDao(currentpage, pagesize);
        int datacount = vehicleDao.findPageInfoDao();
        int pagetatol = (int) Math.ceil(datacount / Double.valueOf(pagesize + ""));
        PageInfo pageInfo = new PageInfo(brands, datacount, pagetatol);
        return pageInfo;
    }

    //修改车辆品牌
    @Override
    public boolean updateBrandService(String s, String brandName) {
        int flag = vehicleDao.updateBrandDao(s, brandName);
        return flag == 1 ? true : false;
    }

    //删除品牌信息
    @Override
    public boolean deleteBrandByNameService(String brandname) {
        int flag = vehicleDao.deleteBrandByNameDao(brandname);
        System.out.println(flag);
        return flag == 1 ? true : false;
    }

    //查找品牌名字
    @Override
    public List<String> findBrandNameService() {
        List<String> brandName = new ArrayList<>();
        List<Brand> brands = vehicleDao.fanBrandNameDao();
        for (int i = 0; i < brands.size(); i++) {
            brandName.add(brands.get(i).getBrandname());
        }
        return brandName;
    }

    //通过名字查找品牌id
    @Override
    public int findBrandIdService(String brand_select) {
        List<Brand> brandByNameDao = vehicleDao.findBrandByNameDao(brand_select);
        if (brandByNameDao.size() == 0) {
            return 0;
        }
        return brandByNameDao.get(0).getId();
    }

    @Override
    public String findOneBrandNameService(int brandid) {
        List<Brand> brands = vehicleDao.findOneBrandNameDao(brandid);
        return null;
    }


    //添加车辆
    @Override
    public boolean addCarService(Car car) {
        int flag = vehicleDao.addCarDao(car);
        return flag == 1 ? true : false;
    }

    //查找所有车辆
    @Override
    public PageInfo<Car> findAllCarService(int currentpage, int pagesize, String brandname, String people) {
        Integer brandid = 0;
        if (!brandname.equals("1")) {
            System.out.println(brandname);
            brandid = findBrandIdService(brandname);
        }
        if (people.equals("0")) {
            people = "";
        }
        System.out.println(brandid);
        List<Car> cars = vehicleDao.findAllCarDao(currentpage, pagesize, brandid, people);
        int datacount = vehicleDao.findCountCarDao(brandid, people);
        String brandname1 = "";
        for (int i = 0; i < cars.size(); i++) {
            List<Brand> oneBrandNameDao = vehicleDao.findOneBrandNameDao(cars.get(i).getBrandid());
            brandname1 = oneBrandNameDao.get(0).getBrandname();
            cars.get(i).setBrandname(brandname1);
            cars.get(i).setHealthstutas(cars.get(i).getHealth() == 1 ? "有损" : "无损");
            cars.get(i).setLeasestutas(cars.get(i).getLease() == 1 ? "在租" : "在库");
        }
        int pagetotal = (int) Math.ceil(datacount / Double.valueOf(pagesize + ""));
        PageInfo<Car> pageInfo = new PageInfo<>(cars, datacount, pagetotal);
        return pageInfo;
    }

    //查询所有报废车辆
    @Override
    public PageInfo<Car> findAllCarBadService(int currentpage, int pagesize, String brandname, String people) {
        Integer brandid = 0;
        if (!brandname.equals("1")) {
            brandid = findBrandIdService(brandname);
        }
        if (people.equals("0")) {
            people = "";
        }
        //System.out.println(brandid);
        List<Car> cars = vehicleDao.findAllCarBadDao(currentpage, pagesize, brandid, people);
        int datacount = vehicleDao.findCountCarBadDao(brandid, people);
        String brandname1 = "";
        for (int i = 0; i < cars.size(); i++) {
            List<Brand> oneBrandNameDao = vehicleDao.findOneBrandNameDao(cars.get(i).getBrandid());
            brandname1 = oneBrandNameDao.get(0).getBrandname();
            cars.get(i).setBrandname(brandname1);
            cars.get(i).setHealthstutas(cars.get(i).getHealth() == 1 ? "车损" : "无损");
            cars.get(i).setLeasestutas(cars.get(i).getLease() == 1 ? "在租" : "在库");
        }
        int pagetotal = (int) Math.ceil(datacount / Double.valueOf(pagesize + ""));
        PageInfo<Car> pageInfo = new PageInfo<>(cars, datacount, pagetotal);
        return pageInfo;
    }

    //查看车损信息
    @Override
    public CarBad SeeCarBadMesService(Integer carId) {
        return vehicleDao.SeeCarBadMesDao(carId);
    }



    //车辆报损
    @Override
    public boolean badCarService(String badPart, String card, String describe){
        return vehicleDao.badCarDao(badPart,card,describe);
    }

    @Override
    public boolean deleteCarByIdService(String id) {
        int flag=vehicleDao.deleteCarByIdDao(Integer.parseInt(id));
        return flag==1?true:false;
    }

    @Override
    public boolean updateCarService(String carid, Car car) {
        int flag=vehicleDao.updateCarDao(Integer.parseInt(carid),car);
        return flag==1?true:false;
    }

    @Override
    public List<Car> findAllByLeaseService(int lease) {
        return vehicleDao.findAllByLeaseDao(lease);
    }

    //修改报损车的时候展示修改车的信息
    @Override
    public Car modifyBadCarService(int carId) {
        return vehicleDao.modifyBadCarDao(carId);
    }

    //修改报损车信息
    @Override
    public boolean realModify(Car car) {
        return vehicleDao.realModify(car);
    }

    //查询所有品牌
    @Override
    public List<Brand> findAllBrand() {
        return vehicleDao.findAllBrand();
    }

    //查询所有门店
    @Override
    public List<Store> findAllStore() {
        return vehicleDao.findAllStore();
    }

    //根据品牌名查询品牌id
    @Override
    public int findOneBrandIdByBrandName(String brandName) {
        return vehicleDao.findOneBrandIdByBrandName(brandName);
    }

    //根据门店名查询门店id
    @Override
    public int findOneStoreIdByBrandName(String storeName) {
        return vehicleDao.findOneStoreIdByBrandName(storeName);
    }

    //删除car表中报损车
    @Override
    public void deleteBadCarSercice(int carId) {
        vehicleDao.deleteBadCarDao(carId);
    }

    //删除carbad表中的报损车
    @Override
    public void deleteBadCarMesSercice(int carId) {
        vehicleDao. deleteBadCarMesDao(carId);
    }

    //修复car表中的报损车
    @Override
    public void mendBadCarSercice(int carId) {
        vehicleDao.mendBadCarSercice(carId);
    }

    //下载所有报损车信息
    @Override
    public List<Car> downloadFindAllCarBadService() {
        return vehicleDao.downloadFindAllCarBadDao();
    }


}
