package com.hzdl.yxzc.service;

import com.hzdl.yxzc.domain.PageBean;
import com.hzdl.yxzc.domain.Store;

import java.util.List;

/**
 * 门店模块
 */
public interface ShopService {
    PageBean<Store> findAllStore(String currentPage, String storeName, String manager);
    void addStore(Store store);

    void deleteStore(String id);

    void updateStore(Store store);

    List getAllStoreName();
}
