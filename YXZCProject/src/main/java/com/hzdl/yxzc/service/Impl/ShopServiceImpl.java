package com.hzdl.yxzc.service.Impl;

import com.hzdl.yxzc.dao.Impl.ShopDaoImpl;
import com.hzdl.yxzc.dao.ShopDao;
import com.hzdl.yxzc.domain.PageBean;
import com.hzdl.yxzc.domain.Store;
import com.hzdl.yxzc.service.ShopService;

import java.util.ArrayList;
import java.util.List;

/**
 * 门店模块
 */
public class ShopServiceImpl implements ShopService {
    ShopDao shopDao=new ShopDaoImpl();
    PageBean pageBean=new PageBean();
    @Override
    public PageBean<Store> findAllStore(String currentPage,String storeName,String manager) {
        if (currentPage==null||currentPage==""){
            currentPage="1";
        }
        int i = Integer.parseInt(currentPage);
        List<Store> list=shopDao.findStore(i,storeName,manager);
        //List<Store> list=shopDao.findAllStore(i);
        int totalCount=shopDao.countStore(storeName,manager);
        pageBean.setTotalCount(totalCount);
        pageBean.setTotalPage(totalCount%5==0?totalCount/5:totalCount/5+1);
        pageBean.setPageList(list);
        pageBean.setRows(5);
        pageBean.setCurrentPage(i);
        return pageBean;
    }

    @Override
    public void addStore(Store store) {
        shopDao.addStore(store);
    }

    @Override
    public void deleteStore(String id) {
        shopDao.deleteStore(id);
    }

    @Override
    public void updateStore(Store store) {
        shopDao.updateStore(store);
    }

    @Override
    public List<Store> getAllStoreName() {
        List storeList=shopDao.getAllStoreName();
        return storeList;
    }
}
