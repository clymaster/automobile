package com.hzdl.yxzc.service;

import com.hzdl.yxzc.domain.*;

import java.util.List;

/**
 * 车辆模块
 */
public interface VehicleService {
    //品牌添加
    boolean addBrandService(String brandname);

    //检索所有品牌
    PageInfo findAllBrandService(int currentpage, int pagesize);

    //修改车辆品牌
    boolean updateBrandService(String s, String brandName);

    //删除品牌信息
    boolean deleteBrandByNameService(String brandname);

    //查找品牌名字
    List<String> findBrandNameService();

    //通过品牌名字查找id
    int findBrandIdService(String brand_select);

    //通过id查找品牌名字
    String findOneBrandNameService(int brandid);

    //添加车辆
    boolean addCarService(Car car);

    //查询所有车辆
    PageInfo<Car> findAllCarService(int currentpage, int pagesize, String brandname, String people);

    //查询所有报废车辆 //my
    PageInfo<Car> findAllCarBadService(int currentpage, int pagesize, String brandname, String people);

    //查看车损信息
    CarBad SeeCarBadMesService(Integer carId);

    //车辆报损
    boolean badCarService(String badPart, String card, String describe);

    //删除车辆通过id
    boolean deleteCarByIdService(String id);

    //修改车辆通过id
    boolean updateCarService(String carid, Car car);

    //通过出租状态查询所有车辆
    List<Car> findAllByLeaseService(int lease);

    //查询所有报废车辆
    Car modifyBadCarService(int carId);

    boolean realModify(Car car);

    //查询所有品牌名
    List<Brand> findAllBrand();

    //查询所有门店
    List<Store> findAllStore();

    //根据品牌名查询品牌id
    int findOneBrandIdByBrandName(String brandName);

    //根据门店名查询门店id
    int findOneStoreIdByBrandName(String storeName);

    //删除car表中报损车辆
    void deleteBadCarSercice(int carId);

    //删除carbad表中的报损车
    void deleteBadCarMesSercice(int carId);

    //修复car表中的报损车
    void mendBadCarSercice(int carId);

    //下载所有报损车信息
    List<Car> downloadFindAllCarBadService();

}
