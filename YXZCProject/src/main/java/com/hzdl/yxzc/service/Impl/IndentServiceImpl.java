package com.hzdl.yxzc.service.Impl;

import com.hzdl.yxzc.dao.Impl.IndentDaoImpl;
import com.hzdl.yxzc.dao.Impl.MemberDaoImpl;
import com.hzdl.yxzc.dao.Impl.ShopDaoImpl;
import com.hzdl.yxzc.dao.IndentDao;
import com.hzdl.yxzc.dao.MemberDao;
import com.hzdl.yxzc.dao.ShopDao;
import com.hzdl.yxzc.domain.Customer;
import com.hzdl.yxzc.domain.DingDan;
import com.hzdl.yxzc.domain.PageInfo;
import com.hzdl.yxzc.domain.Store;
import com.hzdl.yxzc.service.IndentService;

import java.util.List;

/**
 * 订单模块
 */
public class IndentServiceImpl implements IndentService {

    IndentDao indentDao = new IndentDaoImpl();



    @Override
    public DingDan messOrder(String id) {
        //1.根据id去查询订单表中的信息     dingdan
        DingDan dingDan = indentDao.messOrder(id);
        //2.根据costomerId去查询会员表中的信息    costomer
        MemberDao memberDao = new MemberDaoImpl();
        Customer customer = memberDao.findCustomerId(dingDan.getCustomerId());
        //3.根据storeId去查询store表中的信息       store
        ShopDao shopDao = new ShopDaoImpl();
        Store store = shopDao.findStoreId(dingDan.getStoreId());

        //4.将获取到的信息放入dingdan表中
        dingDan.setStore(store);
        dingDan.setCustomer(customer);
        return dingDan;
    }


    @Override
    public PageInfo payPageQuery(String rows, String currentPage,String cusName,String phone,String startTime,String endTime) {
        //1.调用indentDao的payPageQuery方法返回一个订单的list集合
         List<DingDan> list = indentDao.payPageQuery(rows, currentPage,cusName,phone,startTime,endTime);

        //2.a总条数
        int datacount = indentDao.queryDataCount(cusName,phone,startTime,endTime);
        //2.b总页数
        int pagetatol = datacount % Integer.parseInt(rows) == 0 ? datacount / Integer.parseInt(rows) :datacount / Integer.parseInt(rows)+1;

        //3.将分页数据存储到pageInfo中
        PageInfo pageInfo = new PageInfo();
        pageInfo.setDatacount(datacount);
        pageInfo.setPagetatol(pagetatol);
        pageInfo.setBrands(list);

        return pageInfo;
    }

    @Override
    public PageInfo successPageQuery(String rows, String currentPage, String cusName, String phone, String startTime, String endTime) {
        //1.调用indentDao的payPageQuery方法返回一个订单的list集合
        List<DingDan> list = indentDao.successPageQuery(rows, currentPage,cusName,phone,startTime,endTime);

        //2.a总条数
        int datacount = indentDao.queryDataCount(cusName,phone,startTime,endTime);
        //2.b总页数
        int pagetatol = datacount % Integer.parseInt(rows) == 0 ? datacount / Integer.parseInt(rows) :datacount / Integer.parseInt(rows)+1;

        //3.将分页数据存储到pageInfo中
        PageInfo pageInfo = new PageInfo();
        pageInfo.setDatacount(datacount);
        pageInfo.setPagetatol(pagetatol);
        pageInfo.setBrands(list);
        return pageInfo;
    }

    @Override
    public DingDan goonOrder(String id) {
         DingDan dingDan = indentDao.goonOrder(id);
        return dingDan;
    }

    @Override
    public DingDan returnCar(String id) {
        DingDan dingDan = indentDao.returnCar(id);
        return dingDan;
    }

    @Override
    public Boolean goonOrderEndTime(String id,String endTime) {
         Boolean flag = indentDao.goonOrderEndTime(id,endTime);
        return flag;
    }

    @Override
    public boolean returnStore(String id, String returnStore) {
        boolean flag = indentDao.returnStore(id,returnStore);
        return flag;
    }

    @Override
    public int findStatusByCarIdService(String carId) {
        List<DingDan> list = indentDao.findStatusByCarIdService(carId);
        Integer status = list.get(0).getStatus();
        return status;
    }




}
