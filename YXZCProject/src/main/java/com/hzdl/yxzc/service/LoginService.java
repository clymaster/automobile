package com.hzdl.yxzc.service;

import com.hzdl.yxzc.domain.Emp;

/**
 * 登录模块
 */
public interface LoginService {
    Emp empLogin(String username, String password);

    Emp findOnePvm(String empName, String tel);
}
