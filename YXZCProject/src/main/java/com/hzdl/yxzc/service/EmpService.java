package com.hzdl.yxzc.service;

import com.hzdl.yxzc.domain.Emp;
import com.hzdl.yxzc.domain.PageInfo;

import java.util.List;

/**
 * 员工模块
 */
public interface EmpService {
    boolean save(Emp emp);

    PageInfo queryByName(String name, String currentpage, String rows);

    void delectById(String id);

    List queryById(String id);

    boolean modify(Emp emp);

    String findEmpNameByIdService(int id);

    List findAllUserPas();
    //String findEmpName(String id);
}
