package com.hzdl.yxzc.service.Impl;

import com.hzdl.yxzc.dao.Impl.LoginDaoImpl;
import com.hzdl.yxzc.dao.LoginDao;
import com.hzdl.yxzc.domain.Emp;
import com.hzdl.yxzc.service.LoginService;

/**
 * 登录模块
 */
public class LoginServiceImpl implements LoginService {
   LoginDao loginDao = new LoginDaoImpl();
    @Override
    public Emp empLogin(String username, String password) {
        return loginDao.empLogin(username,password);
    }

    @Override
    public Emp findOnePvm(String empName, String tel) {
        return loginDao.findOnePvm(empName,tel);
    }
}
