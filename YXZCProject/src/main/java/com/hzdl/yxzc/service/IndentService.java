package com.hzdl.yxzc.service;

import com.hzdl.yxzc.domain.DingDan;
import com.hzdl.yxzc.domain.PageInfo;

import java.util.List;

/**
 * 订单模块
 */
public interface IndentService {

    //订单详情
    DingDan messOrder(String id);

    //在租订单分页模糊查询
    PageInfo payPageQuery(String rows, String currentPage, String cusName, String phone, String startTime, String endTime);

    //已完成订单分页模糊查询
    PageInfo successPageQuery(String rows, String currentPage, String cusName, String phone, String startTime, String endTime);

    //续租订单
    DingDan goonOrder(String id);

    //获取续租订单修改的时间
    Boolean  goonOrderEndTime(String id, String endTime);

    //通过carId字段查找订单表status字段
    int findStatusByCarIdService(String carId);

    //归还车辆信息修改还车门店
    boolean returnStore(String id, String returnStore);

    //归还车辆信息
    DingDan returnCar(String id);
}
