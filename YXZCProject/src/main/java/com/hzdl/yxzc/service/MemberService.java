package com.hzdl.yxzc.service;

import com.hzdl.yxzc.domain.*;

import java.util.List;

/**
 * 会员servlet
 */
public interface MemberService {
    boolean saveadd(Customer customer);

    List<Customer> findCustomerByPhone(String phone);

    List<Brand> findAllBrand();

    List<Emp> findAllEmp();

    List<List<Store>> findStoreBybrandName(String brandname);

    ResultInfo addOder(Order order);

    //根据id查询
    Customer customerById(String id);


    PageBean<Customer> findcusByPage(String currentPage, String rows);


    void updatevip(Customer customer);
}
