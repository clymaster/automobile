package com.hzdl.yxzc.service.Impl;

import com.hzdl.yxzc.dao.Impl.MemberDaoImpl;
import com.hzdl.yxzc.dao.MemberDao;
import com.hzdl.yxzc.domain.*;
import com.hzdl.yxzc.service.MemberService;
import com.hzdl.yxzc.util.CountDay;

import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 会员servlet
 */
public class MemberServiceImpl implements MemberService {
    MemberDao memberDao=new MemberDaoImpl();
    @Override
    public boolean saveadd(Customer customer) {
        return memberDao.saveadd(customer);
    }

    @Override
    public List<Customer> findCustomerByPhone(String phone) {
        List<Customer> customer=memberDao.findCustomerByPhone(phone);
        return customer;
    }

    @Override
    public List<Brand> findAllBrand() {
        List<Brand> brandList=memberDao.findAllBrand();
        return  brandList;
    }

    @Override
    public List<Emp> findAllEmp() {
        List<Emp> empList=memberDao.findAllEmp();
        return empList;
    }

    @Override
    public List<List<Store>> findStoreBybrandName(String brandname) {
        List<List<Store>> storeList=memberDao.findStoreBybrandName(brandname);
        return storeList;
    }

    @Override
    public ResultInfo addOder(Order order) {
        DingDan dingDan=new DingDan();
        String s = String.valueOf(System.currentTimeMillis());
        dingDan.setOrderId("c-"+s);
        Customer customer=memberDao.findOneCustomerByPhone(order.getMemberPhone());
        dingDan.setCustomerId(customer.getId());
        List<List<Car>> carList=memberDao.findCar(order.getBrandName(),order.getStoreName());
        for (List<Car> list : carList) {
            for (Car car : list) {
                int id = car.getId();
                String price = car.getPrice();
                dingDan.setCarId(id);
                dingDan.setPrice(Double.valueOf(price));
                //名爵MG5 | 两厢1.6L手动5座
                //brandName,type,style,output,bsx,people
                dingDan.setCarMess(order.getBrandName()+car.getType()+"|"+car.getStyle()+car.getOutput()+car.getBsx()+car.getPeople()+"座");
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//yyyy-MM-dd HH:mm:ss
        try {
            dingDan.setStartTime(sdf.parse(order.getStartDate()));
            dingDan.setEndTime(sdf.parse(order.getEndDate()));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        dingDan.setEmpName(order.getEmpName());
        dingDan.setTime(CountDay.getDay(order.getStartDate(),order.getEndDate()));
        dingDan.setTotal(dingDan.getTime()*dingDan.getPrice());
        dingDan.setStatus(1);
        dingDan.setBorrowStore(order.getStoreName());
        dingDan.setReturnStore(order.getStoreName());
        Emp emp=memberDao.findEmp(order.getEmpName());
        dingDan.setEmpId(emp.getId());
        //Brand brand = memberDao.findBrand(order.getBrandName());
       Store store=memberDao.findStoreByStoreName(order.getStoreName());
       dingDan.setStoreId(store.getId());
       dingDan.setCusName(customer.getCusName());
       dingDan.setPhone(order.getMemberPhone());
       dingDan.setCreateTime(new Date());

        DingDan dingDan1=memberDao.addOder(dingDan);
       ResultInfo resultInfo=new ResultInfo();
       resultInfo.setFlag(true);
       resultInfo.setData(dingDan1);
       resultInfo.setErrorMsg("租车成功！");
        return resultInfo;
    }
    @Override
    public Customer customerById(String id) {
        return memberDao.ById(Integer.parseInt(id));
    }

    @Override
    public PageBean<Customer> findcusByPage(String _currentPage, String _rows) {
        if (_currentPage==""){
            _currentPage="0";
        }

        int currentPage = Integer.parseInt(_currentPage);

        int rows = Integer.parseInt(_rows);

        //1.创建空的PageBean对象
        PageBean<Customer> pb= new PageBean<Customer>();
        //2.设置参数
        pb.setCurrentPage(currentPage);
        pb.setRows(rows);


        //3.调用dao查询总记录数
        int totalCount= memberDao.findTotalCount();
        pb.setTotalCount(totalCount);
        //4.调用dao查询list集合
        //计算开始的记录索引
        int start =(currentPage-1)*rows;
        if (start<0){
            start=0;
        }


        List<Customer> list =memberDao.findByPage(start,rows);
        pb.setPageList(list);
        //5.计算总页码
        int totalPage =(totalCount % rows) ==0 ? totalCount/rows : (totalCount/rows+1);
        pb.setTotalPage(totalPage);

        return pb;


    }

        @Override
        public void updatevip(Customer customer) {
        memberDao.updatevip(customer);
    }
}

