package com.hzdl.yxzc.dao;

import com.hzdl.yxzc.domain.DingDan;

import java.util.List;

/**
 * 订单模块
 */
public interface IndentDao {

    DingDan messOrder(String id);

    List<DingDan> payPageQuery(String rows, String currentPage, String cusName, String phone, String startTime, String endTime);

    int queryDataCount(String cusName, String phone, String startTime, String endTime);

    List<DingDan> successPageQuery(String rows, String currentPage, String cusName, String phone, String startTime, String endTime);

    DingDan goonOrder(String id);

    DingDan returnCar(String id);

    Boolean goonOrderEndTime(String id, String endTime);

    boolean returnStore(String id, String returnStore);

    List<DingDan> findStatusByCarIdService(String carId);


}
