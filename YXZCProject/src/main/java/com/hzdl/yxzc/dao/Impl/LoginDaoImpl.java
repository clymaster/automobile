package com.hzdl.yxzc.dao.Impl;

import com.hzdl.yxzc.dao.LoginDao;
import com.hzdl.yxzc.domain.Emp;
import com.hzdl.yxzc.util.JDBCUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * 登录模块
 */
public class LoginDaoImpl implements LoginDao {
    JdbcTemplate jdbcTemplate = new JdbcTemplate(JDBCUtils.getDataSource());

    @Override
    public Emp empLogin(String username, String password) {
        String sql = "select * from emp where empName = ? and password = ?";
        Emp emp = null;
        try {
            emp = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Emp.class), username, password);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return emp;
    }

    @Override
    public Emp findOnePvm(String empName, String tel) {
        String sql = "select * from emp where empName = ? and phone = ?";
        Emp emp = null;
        try {
            emp = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Emp.class), empName, tel);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return emp;
    }
}
