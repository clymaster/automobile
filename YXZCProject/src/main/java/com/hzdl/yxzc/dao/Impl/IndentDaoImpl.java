package com.hzdl.yxzc.dao.Impl;

import com.hzdl.yxzc.dao.IndentDao;
import com.hzdl.yxzc.domain.DingDan;
import com.hzdl.yxzc.util.JDBCUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;


/**
 * 订单模块
 */
public class IndentDaoImpl implements IndentDao {

    JdbcTemplate jdbcTemplate = new JdbcTemplate(JDBCUtils.getDataSource());



    @Override
    public DingDan messOrder(String id) {
        String sql = "select * from dingdan where id = ?";
        DingDan dingDan = null;
        try {
             dingDan = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(DingDan.class), Integer.parseInt(id));
        } catch (DataAccessException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return dingDan;
    }
    @Override
    public DingDan returnCar(String id) {
        String sql = "select * from dingdan where id = ?";
        DingDan dingDan = null;
        try {
            dingDan = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(DingDan.class), Integer.parseInt(id));
        } catch (DataAccessException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return dingDan;
    }

    @Override
    public List<DingDan> payPageQuery(String rows, String currentPage,String cusName,String phone,String startTime,String endTime) {
        int index = ( Integer.parseInt(currentPage.trim()) - 1) *Integer.parseInt(rows.trim());
        String sql = "select * from dingdan where IF('null'=?  or '' = ?,1=1,cusName like concat('%',?,'%')) and IF('null'=?  or '' = ?,1=1,startTime >?) and IF('null'=?  or '' = ?,1=1,endTime <?)and IF('null'= ? or '' = ?,1=1,phone = ?) and status = 1 limit ?,?";
        List<DingDan> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(DingDan.class),cusName,cusName,cusName,startTime,startTime,startTime,endTime,endTime,endTime,phone,phone,phone,index, Integer.parseInt(rows));
        return  list;
    }

    @Override
    public int queryDataCount(String cusName,String phone,String startTime,String endTime) {
        String sql = "select count(*) from dingdan where IF('null'=?  or '' = ?,1=1,cusName like concat('%',?,'%')) and IF('null'=?  or '' = ?,1=1,startTime >?) and IF('null'=?  or '' = ?,1=1,endTime <?)and IF('null'= ? or '' = ?,1=1,phone = ?)";
        Integer count = jdbcTemplate.queryForObject(sql, Integer.class,cusName,cusName,cusName,startTime,startTime,startTime,endTime,endTime,endTime,phone,phone,phone);
        return count;
    }

    @Override
    public List<DingDan> successPageQuery(String rows, String currentPage, String cusName, String phone, String startTime, String endTime) {
        int index = (Integer.parseInt(rows)) * (Integer.parseInt(currentPage) - 1);
        String sql = "select * from dingdan where if('null' = ? or '' = ?,1=1,cusName like concat('%',?,'%')) and if('null' = ? or '' = ? ,1=1,startTime>?) and if('null' = ? or '' = ? ,1=1,endTime<?) and if('null' = ? '' = ? ,1=1,phone = ?)and status = 0 limit ?,?";
        List<DingDan> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(DingDan.class),cusName,cusName,cusName,startTime,startTime,startTime,endTime,endTime,endTime,phone,phone,phone,index, Integer.parseInt(rows));
        return list;
    }

    @Override
    public DingDan goonOrder(String id) {
        String sql = "select * from dingdan where id = ? ";
        DingDan dingDan = null;
        try {
            dingDan = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(DingDan.class), Integer.parseInt(id));
        } catch (DataAccessException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return dingDan;
    }

    @Override
    public Boolean goonOrderEndTime(String id,String endTime) {
        String sql = "update dingdan set endTime = ? where id = ?";
        int update = jdbcTemplate.update(sql,endTime, Integer.parseInt(id));
        return update > 0 ? true:false;
    }

    @Override
    public boolean returnStore(String id, String returnStore) {
        String sql = "update dingdan set returnStore = ?, status = 0  where id = ?  and status = 1";
        int update = jdbcTemplate.update(sql, returnStore, Integer.parseInt(id));
        return update > 0 ? true:false;
    }

    @Override
    public List<DingDan> findStatusByCarIdService(String carId) {
        String sql = "select * from dingdan where carId = ? ";
        List<DingDan> query = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(DingDan.class), carId);
        return query;
    }


}
