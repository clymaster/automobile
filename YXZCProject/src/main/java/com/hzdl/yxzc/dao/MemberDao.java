package com.hzdl.yxzc.dao;

import com.hzdl.yxzc.domain.*;

import java.util.List;

/**
 * 会员servlet
 */
public interface MemberDao {
    boolean saveadd(Customer customer);

    List<Customer> findCustomerByPhone(String phone);

    List<Brand> findAllBrand();

    List<Emp> findAllEmp();

    List<List<Store>> findStoreBybrandName(String brandname);

    Customer findOneCustomerByPhone(String memberPhone);

    List<List<Car>> findCar(String brandName, String storeName);

    Emp findEmp(String empName);

    Brand findBrand(String brandName);

    Store findStoreByStoreName(String storeName);

    DingDan addOder(DingDan dingDan);

    boolean add(Customer customer);

    Customer ById(int id);

    //找到customer的id
    Customer findCustomerId(int id);

    //查询总记录数
    int findTotalCount();
    //分页查询每页记录
    List<Customer> findByPage(int start, int rows);


    void updatevip(Customer customer);
}
