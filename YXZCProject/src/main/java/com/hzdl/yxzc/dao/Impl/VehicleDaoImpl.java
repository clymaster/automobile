package com.hzdl.yxzc.dao.Impl;

import com.hzdl.yxzc.dao.VehicleDao;
import com.hzdl.yxzc.domain.*;
import com.hzdl.yxzc.util.JDBCUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 车辆模块
 */
public class VehicleDaoImpl implements VehicleDao {
    JdbcTemplate jdbcTemplate = new JdbcTemplate(JDBCUtils.getDataSource());

    //品牌添加
    @Override
    public int addBrandDao(String brandname) {
        String sql = "insert into brand (brandName,createTime) values (?,now())";
        int update = (int) jdbcTemplate.update(sql, brandname);
        return update;
    }

    //通过名字查询一个品牌
    @Override
    public List<Brand> findBrandByNameDao(String brandname) {
        String sql = "select * from brand where brandName=?";
        List<Brand> query = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Brand.class), brandname);
        return query;
    }

    //检索所有品牌
    @Override
    public List<Brand> findAllBrandDao(int currentpage, int pagesize) {
        int i = (currentpage - 1) * pagesize;
        String sql = "select * from brand limit ?,?";
        List<Brand> query = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Brand.class), i, i + pagesize);
        return query;
    }

    //分页查询
    @Override
    public int findPageInfoDao() {
        String sql = "select count(*) from brand";
        Integer integer = jdbcTemplate.queryForObject(sql, Integer.class);
        return integer;
    }

    //修改车辆品牌
    @Override
    public int updateBrandDao(String s, String brandName) {
        String sql = "update brand set brandName=? where brandName=?";
        int update = jdbcTemplate.update(sql, s, brandName);
        return update;
    }

    //删除品牌信息
    @Override
    public int deleteBrandByNameDao(String brandname) {
        String sql = "delete from brand where brandName=?";
        int update = jdbcTemplate.update(sql, brandname);
        return update;
    }

    //查找品牌名字
    @Override
    public List<Brand> fanBrandNameDao() {
        String sql = "select * from brand";
        List<Brand> query = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Brand.class));
        return query;
    }

    //添加车辆
    @Override
    public int addCarDao(Car car) {
        String sql = "insert into car (brandId,type,style,bsx,output,people,price,card,storeId,lease,health,createTime) values (?,?,?,?,?,?,?,?,?,?,?,now())";
        int update = jdbcTemplate.update(sql, car.getBrandid(),
                car.getType(),
                car.getStyle(),
                car.getBsx(),
                car.getOutput(),
                car.getPeople(),
                car.getPrice(),
                car.getCard(),
                car.getStoreId(),
                0, 0);
        return update;
    }

    //查找车辆
    @Override
    public List<Car> findAllCarDao(int currentpage, int pagesize, Integer brandid, String people) {
        int i = (currentpage - 1) * pagesize;
        String sql = "";
        List<Car> query = new ArrayList<>();
        if (brandid == 0) {
            sql = "select * from car where people like concat('%',?,'%') limit ?,?";
            query = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Car.class), people, i, i + pagesize);
        } else {
            sql = "select * from car where brandid like concat('%',?,'%') and people like concat('%',?,'%') limit ?,?";
            query = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Car.class), brandid, people, i, i + pagesize);
        }
        return query;
    }

    @Override
    public int findCountCarDao(Integer brandid, String people) {
        String sql = "";
        int integer = 0;
        if (brandid == 0) {
            sql = "select count(*) from car where people like concat('%',?,'%')";
            integer = jdbcTemplate.queryForObject(sql, Integer.class, people);
        } else {
            sql = "select count(*) from car where brandid like concat('%',?,'%') and people like concat('%',?,'%')";
            integer = jdbcTemplate.queryForObject(sql, Integer.class, brandid, people);
        }
        return integer;
    }

    @Override
    public List<Brand> findOneBrandNameDao(int brandid) {
        String sql = "select * from brand where id=?";
        List<Brand> query = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Brand.class), brandid);
        return query;
    }

    //查找所有报废车辆  //ly
    @Override
    public List<Car> findAllCarBadDao(int currentpage, int pagesize, Integer brandid, String people) {
        int i = (currentpage - 1) * pagesize;
        String sql = "";
        List<Car> query = new ArrayList<>();
        if (brandid == 0) {
            sql = "select * from car where health = 1 and people like concat('%',?,'%') limit ?,?";
            query = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Car.class), people, i, i + pagesize);
        } else {
            sql = "select * from car where health = 1 and brandid like concat('%',?,'%') and people like concat('%',?,'%') limit ?,?";
            query = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Car.class), brandid, people, i, i + pagesize);
        }
        return query;
    }


    //统计所有报废车辆的总数 //ly
    @Override
    public int findCountCarBadDao(Integer brandid, String people) {
        String sql = "";
        int integer = 0;
        if (brandid == 0) {
            sql = "select count(*) from car where health = 1 and people like concat('%',?,'%')";
            integer = jdbcTemplate.queryForObject(sql, Integer.class, people);
        } else {
            sql = "select count(*) from car where health = 1 and brandid like concat('%',?,'%') and people like concat('%',?,'%')";
            integer = jdbcTemplate.queryForObject(sql, Integer.class, brandid, people);
        }
        return integer;
    }


    //查看车辆报废信息  //ly
    @Override
    public CarBad SeeCarBadMesDao(Integer carId) {
        String sql = "select * from carbad where carId = ?";
        CarBad carBad = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(CarBad.class), carId);
        return carBad;
    }

    //车辆报损
    //1.查询是否有报损的这辆车 并返回这辆车的id
    @Override
    public int isExistBadCarDao(String card) {
        String sql = "select id from car where card = ? and health = 0";
        int carId = 0;
        try {
            carId = jdbcTemplate.queryForObject(sql, Integer.class, card);
        } catch (DataAccessException e) {
            //e.printStackTrace();
        }
        if (carId == 0) {

        }
        return carId;

    }

    //修改车的health状态
    @Override
    public void healthToBad(String card) {
        String sql = "update car set health = 1 where card = ?";
        jdbcTemplate.update(sql, card);

    }

    @Override
    public boolean badCarDao(String badPart, String card, String describe) {
        String sql = null;
        int carId = isExistBadCarDao(card);
        if (isExistBadCarDao(card) == 0) {
            //不存在这辆车或车已经报废过了
            return false;
        } else {
            //存在这辆车且未报废过
            healthToBad(card);
            sql = "insert into carbad (carId,part,mess) values (?,?,?)";
            jdbcTemplate.update(sql, carId, badPart, describe);
        }
        return true;
    }


    @Override
    public int deleteCarByIdDao(int i) {
        String sql = "delete from car where id=?";
        Integer integer = jdbcTemplate.update(sql, i);
        return integer;
    }

    @Override
    public int updateCarDao(int i, Car car) {
        String sql = "update car set brandId=?,type=?,style=?,bsx=?,output=?,people=?,price=?,card=?,storeId=?,lease=?,health=?,createTime=now() where id=?";
        int update = jdbcTemplate.update(sql,
                car.getBrandid(),
                car.getType(),
                car.getStyle(),
                car.getBsx(),
                car.getOutput(),
                car.getPeople(),
                car.getPrice(),
                car.getCard(),
                car.getStoreId(), 0, 0, i);
        return update;
    }

    //通过出租状态查询所有车辆
    @Override
    public List<Car> findAllByLeaseDao(int lease) {
        String sql = "select * from car where lease = ?";
        List<Car> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Car.class), lease);
        return list;
    }

    //查看被修改报废车辆的信息
    @Override
    public Car modifyBadCarDao(int carId) {
        String sql = "select * from car where id = ?";
        Car car = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Car.class), carId);
        return car;
    }

    //修改报损车辆的信息
    @Override
    public boolean realModify(Car car) {
        String sql = "update car set brandId = ?,style=?,bsx=?,people=?,card=?,type = ?,output=?,price=? ,storeId = ? where id = ?";
        int update = jdbcTemplate.update(sql, car.getBrandid(), car.getStyle(), car.getBsx(), car.getPeople(), car.getCard(), car.getType(), car.getOutput(), car.getPrice(), Integer.parseInt(car.getStoreId()), car.getId());
        return update > 0 ? true : false;
    }

    //查询所有品牌
    @Override
    public List<Brand> findAllBrand() {
        String sql = "select * from brand ";
        List<Brand> list_brand = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Brand.class));
        return list_brand;
    }

    //查询所有门店
    @Override
    public List<Store> findAllStore() {
        String sql = "select * from store ";
        List<Store> list_store = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Store.class));
        return list_store;
    }

    //根据品牌名查询品牌id
    @Override
    public int findOneBrandIdByBrandName(String brandName) {
        String sql = "select id from brand where brandName = ?";
        Integer oneBrandId = jdbcTemplate.queryForObject(sql, Integer.class, brandName);
        return oneBrandId;
    }

    //根据门店名查询门店id
    @Override
    public int findOneStoreIdByBrandName(String storeName) {
        String sql = "select id from store where storeName = ?";
        Integer oneStoreId = jdbcTemplate.queryForObject(sql, Integer.class, storeName);
        return oneStoreId;
    }

    //从car表中删除
    @Override
    public void deleteBadCarDao(int carId) {
        String sql = "delete from car where id = ?";
        jdbcTemplate.update(sql, carId);
    }

    //删除carbad表中的报损车
    @Override
    public void deleteBadCarMesDao(int carId) {
        String sql = "delete from carbad where carId = ?";
        jdbcTemplate.update(sql, carId);
    }

    //修复car表中的报损车
    @Override
    public void mendBadCarSercice(int carId) {
        String sql = "update car set health = 0 where id = ?";
        jdbcTemplate.update(sql, carId);
    }

    @Override
    public List<Car> downloadFindAllCarBadDao() {
        String sql="select * from car where health = 1";
        List<Car> cars = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Car.class));
        return cars;
    }


}
