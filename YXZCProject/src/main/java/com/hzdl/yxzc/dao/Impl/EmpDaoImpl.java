package com.hzdl.yxzc.dao.Impl;

import com.hzdl.yxzc.dao.EmpDao;
import com.hzdl.yxzc.domain.Emp;
import com.hzdl.yxzc.service.EmpService;
import com.hzdl.yxzc.util.JDBCUtils;
import com.hzdl.yxzc.util.UuidUtil;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * 员工模块
 */
public class EmpDaoImpl implements EmpDao {
    JdbcTemplate jdbcTemplate = new JdbcTemplate(JDBCUtils.getDataSource());
    @Override
    public boolean save(Emp emp) {
        String sql = "insert into emp values(?,?,?,?,?,?,?,null,?,?,?)";
        //生成一个empId
        String i = UuidUtil.getUuid();
        emp.setEmpId(i);
        int n = jdbcTemplate.update(sql,emp.getEmpId(), emp.getEmpName(), emp.getSex(), emp.getBirthday(), emp.getJoinTime(), emp.getIdcard(), emp.getPhone(), emp.getStoreId(), emp.getPassword(), emp.getStatus());
        return n>0?true:false;
    }

    @Override
    public boolean queryByUsername(Emp emp) {
        //通过用户名查询用户是否存在
        String sql = "select * from emp where empName = ? ";
        try {
            Emp emp1 = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Emp.class), emp.getEmpName());
            if (null!=emp1){
                return true;
            }
        } catch (Exception e) {

        }
        return false;
    }

    @Override
    public List<Emp> queryByName(String name,String currentpage,String rows) {
        int index = (Integer.parseInt(currentpage)-1)*Integer.parseInt(rows);
        String sql = "select * from emp where IF('null'=?,1=1,empName like concat('%',?,'%'))limit ?,?";
        List<Emp> empList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Emp.class), name, name, index, Integer.parseInt(rows));
        return empList;
    }

    @Override
    public int queryCount(String name) {
        String sql = "select count(*) from emp where IF('null'=?,1=1,empName like concat('%',?,'%'))";
        Integer count = jdbcTemplate.queryForObject(sql, Integer.class, name, name);
        return count;
    }

    @Override
    public void delectById(String id) {
        String sql = "delete from emp where id = ?";
        int i = jdbcTemplate.update(sql, Integer.parseInt(id));
        //System.out.println(i);
    }

    @Override
    public List<Emp> queryById(String id) {
        String sql = "select * from emp where id = ?";
        List<Emp> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Emp.class), id);
        return list;
    }

    @Override
    public boolean modify(Emp emp) {
        String sql = "update emp set empName = ?,sex=?,birthday=?,joinTime=?,idcard=?,phone=?,storeId=? where id=? ";
        int i = jdbcTemplate.update(sql, emp.getEmpName(), emp.getSex(), emp.getBirthday(), emp.getJoinTime(), emp.getIdcard(), emp.getPhone(), emp.getStoreId(), emp.getId());
        return i>0?true:false;
    }

    @Override
    public String findEmpNameByIdService(int id) {
        //通过id字段查找员工表empName字段功能
        String sql = "select empName from emp where id=?";
        String s = jdbcTemplate.queryForObject(sql, String.class, id);
        return s;
    }

    @Override
    public List findAllUserPas() {
        //查找所有empName，password字段功能
        String sql = "select * from emp";
        List<Emp> empList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Emp.class));
        return empList;
    }

}
