package com.hzdl.yxzc.dao;

import com.hzdl.yxzc.domain.Emp;

/**
 * 登录模块
 */
public interface LoginDao {
    Emp empLogin(String username, String password);

    Emp findOnePvm(String empName, String tel);
}
