package com.hzdl.yxzc.dao;

import com.hzdl.yxzc.domain.*;

import java.util.List;

/**
 * 车辆模块
 */
public interface VehicleDao {
    //品牌添加
    int addBrandDao(String brandname);

    //通过名字查询一个品牌
    List<Brand> findBrandByNameDao(String brandname);

    //检索所有品牌
    List<Brand> findAllBrandDao(int currentpage, int pagesize);

    //分页查询
    int findPageInfoDao();

    //修改车辆品牌
    int updateBrandDao(String s, String brandName);

    //删除品牌信息
    int deleteBrandByNameDao(String brandname);

    //查找品牌名字
    List<Brand> fanBrandNameDao();

    //添加车辆
    int addCarDao(Car car);

    //查找所有车辆
    List<Car> findAllCarDao(int currentpage, int pagesize, Integer brandid, String people);

    //统计这里总数
    int findCountCarDao(Integer brandid, String people);

    //通过id查找品牌名字
    List<Brand> findOneBrandNameDao(int brandid);

    //统计所有报废车辆的总数 //my
    int findCountCarBadDao(Integer brandid, String people);

    //查找所有报废车辆  //my
    List<Car> findAllCarBadDao(int currentpage, int pagesize, Integer brandid, String people);

    //查看车损信息
    CarBad SeeCarBadMesDao(Integer carId);
    //判断报损车辆是否存在
    int isExistBadCarDao(String card);

    //修改车的health状态
    void healthToBad(String card);

    boolean badCarDao(String badPart, String card, String describe);

    //删除车辆通过id
    int deleteCarByIdDao(int i);

    //修改车辆通过id
    int updateCarDao(int i, Car car);

    //根据租赁状态来查询所有车辆
    List<Car> findAllByLeaseDao(int lease);

    //查看被修改的报废车辆的信息
    Car modifyBadCarDao(int carId);

    //修改报废车辆的信息
    boolean realModify(Car car);

    //查询所有品牌
    List<Brand> findAllBrand();

    //查询所有门店
    List<Store> findAllStore();

    //根据品牌名插查询品牌id
    int findOneBrandIdByBrandName(String brandName);

    //根据门店名查询门店id
    int findOneStoreIdByBrandName(String storeName);

    //删除car表中报损车
    void deleteBadCarDao(int carId);

    //删除carbad表中的报损车
    void deleteBadCarMesDao(int carId);

    //修复car表中的报损车
    void mendBadCarSercice(int carId);

    //下载所有报损车信息
    List<Car> downloadFindAllCarBadDao();

}
