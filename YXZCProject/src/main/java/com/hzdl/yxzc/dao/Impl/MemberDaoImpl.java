package com.hzdl.yxzc.dao.Impl;

import com.hzdl.yxzc.dao.MemberDao;
import com.hzdl.yxzc.domain.*;
import com.hzdl.yxzc.service.MemberService;
import com.hzdl.yxzc.util.JDBCUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 会员servlet
 */
public class MemberDaoImpl implements MemberDao {
    JdbcTemplate jdbcTemplate=new JdbcTemplate(JDBCUtils.getDataSource());
    @Override
    public boolean saveadd(Customer customer) {
        Date date=new Date();
        String sql="insert into customer values (null,?,?,?,?,?,000,?)";
        int update = jdbcTemplate.update(sql, customer.getCusName(), customer.getSex(), customer.getBirthday(), customer.getAdddress(), customer.getPhone(),date);

        return update>0?true:false;
    }

    @Override
    public List<Customer> findCustomerByPhone(String phone) {
        String sql="select * from customer where phone=?";
        //List<Customer> customers = jdbcTemplate.query( sql, new BeanPropertyRowMapper<>( Customer.class ), phone );
        List<Customer> customerList = jdbcTemplate.query( sql, new BeanPropertyRowMapper<>( Customer.class ),phone);
        return customerList;
    }

    @Override
    public List<Brand> findAllBrand() {
        String sql="select * from brand";
        List<Brand> brandList = jdbcTemplate.query( sql, new BeanPropertyRowMapper<>( Brand.class ) );
        return brandList;
    }

    @Override
    public List<Emp> findAllEmp() {
        String sql="select * from emp";
        List<Emp> empList = jdbcTemplate.query( sql, new BeanPropertyRowMapper<>( Emp.class ) );
        return empList;
    }

    @Override
    public List<List<Store>> findStoreBybrandName(String brandname) {


        String sql1="select * from brand where brandName =?";
        Brand brand = jdbcTemplate.queryForObject( sql1, new BeanPropertyRowMapper<>( Brand.class ), brandname );
        int brandId=brand.getId();
        String sql2="select * from car where brandId=?";
        List<Car> carList = jdbcTemplate.query( sql2, new BeanPropertyRowMapper<>( Car.class ), brandId);
        List<List<Store>> storeListList=new ArrayList();
        for (Car car : carList) {
            String storeId = car.getStoreId();
            String sql3="select *from store where Id=?";
            List<Store> storeList = jdbcTemplate.query( sql3, new BeanPropertyRowMapper<>( Store.class ), Integer.parseInt(storeId));
            storeListList.add(storeList);
        }
        return storeListList;
    }

    @Override
    public Customer findOneCustomerByPhone(String memberPhone) {
        String sql="select * from customer where phone=?";
        Customer customer = jdbcTemplate.queryForObject( sql, new BeanPropertyRowMapper<>( Customer.class ), memberPhone );
        return customer;
    }

    @Override
    public  List<List<Car>> findCar(String brandName, String storeName) {
        String sql1="select * from brand where brandName=?";
        String sql2="select * from store where storeName=?";
        Brand brand = jdbcTemplate.queryForObject(sql1,new BeanPropertyRowMapper<>( Brand.class ),brandName);
        List<Store> storeList = jdbcTemplate.query( sql2, new BeanPropertyRowMapper<>( Store.class ), storeName );
       List<List<Car>> carList=new ArrayList<>();
        for (Store store : storeList) {
            int id = store.getId();
            String sql3="select * from car where brandId=? and storeId=?";
            List<Car> car = jdbcTemplate.query( sql3, new BeanPropertyRowMapper<>( Car.class),brand.getId(),id);
            carList.add(car);
        }

        return carList;
    }

    @Override
    public Emp findEmp(String empName) {
        String sql="select *FROM emp where empName=?";
        Emp emp = jdbcTemplate.queryForObject( sql, new BeanPropertyRowMapper<>( Emp.class ), empName );
        return emp;
    }

    @Override
    public Brand findBrand(String brandName) {
        String sql="select *FROM brand where brandName=?";
        Brand brand = jdbcTemplate.queryForObject( sql, new BeanPropertyRowMapper<>( Brand.class ), brandName);
        return brand;
    }

    @Override
    public Store findStoreByStoreName(String storeName) {
        String sql="select * from store where storeName=?";
        Store store = jdbcTemplate.queryForObject( sql, new BeanPropertyRowMapper<>( Store.class ), storeName );
        return store;
    }

    @Override
    public DingDan addOder(DingDan dingDan) {
        String sql="insert into dingdan values (null ,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(sql,dingDan.getCustomerId(),dingDan.getCarId(),
                dingDan.getStartTime(),dingDan.getEndTime(),
                dingDan.getEmpName(),dingDan.getTotal(),dingDan.getOrderId(),
                dingDan.getTime(),dingDan.getStatus(),dingDan.getBorrowStore(),
                dingDan.getReturnStore(),dingDan.getPrice(),dingDan.getEmpId(),
                dingDan.getCarMess(),dingDan.getStoreId(),dingDan.getCreateTime(),
                dingDan.getCusName(),dingDan.getPhone());

        String sql2="select * from dingdan where orderId=?";
        DingDan dingDan1 = jdbcTemplate.queryForObject( sql2, new BeanPropertyRowMapper<>( DingDan.class ), dingDan.getOrderId());
        return dingDan1;
    }
    @Override
    public boolean add(Customer customer) {
        String sql = "insert into customer values (null,?,?,?,?,?,null,null)";
        int update = jdbcTemplate.update(sql, customer.getCusName(), customer.getSex(), customer.getBirthday(), customer.getAdddress(), customer.getPhone());

        return update > 0 ? true : false;

    }

    @Override
    public Customer ById(int id) {

        String sql = "select * from customer where id=?";
        Customer customer = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<Customer>(Customer.class), id);
        return customer;
    }

    @Override
    public int findTotalCount() {
        String sql = "select count(*) from customer";
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public List<Customer> findByPage(int start, int rows) {
        String sql = "select * from customer limit ? , ?";

        List<Customer> customerList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Customer>(Customer.class), start, rows);
        return customerList;
    }

    @Override
    public void updatevip(Customer customer) {
        String sql="update customer set cusName=?,sex=?,birthday=?,adddress=?,phone=?";

        int update = jdbcTemplate.update(sql, customer.getCusName(), customer.getSex(), customer.getBirthday(), customer.getAdddress(), customer.getPhone());
    }

    @Override
    public Customer findCustomerId(int id) {
        String sql = "select * from customer where id = ?";
        Customer customer = null;
        try {
            customer = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Customer.class), id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customer;
    }

}
