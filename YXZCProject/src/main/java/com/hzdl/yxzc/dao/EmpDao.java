package com.hzdl.yxzc.dao;

import com.hzdl.yxzc.domain.Emp;

import java.util.List;

/**
 * 员工模块
 */
public interface EmpDao {
    boolean save(Emp emp);

    boolean queryByUsername(Emp emp);

    List<Emp> queryByName(String name, String currentpage, String rows);

    int queryCount(String name);

    void delectById(String id);

    List<Emp> queryById(String id);

    boolean modify(Emp emp);

    String findEmpNameByIdService(int id);

    List findAllUserPas();



    //String queryById(String id);

    //String findEmpName(String id);
}
