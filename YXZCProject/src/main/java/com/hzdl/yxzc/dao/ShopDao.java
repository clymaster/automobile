package com.hzdl.yxzc.dao;

import com.hzdl.yxzc.domain.Store;

import java.util.List;

/**
 * 门店模块
 */
public interface ShopDao {
    List<Store> findAllStore(int i);

    void addStore(Store store);

    void deleteStore(String id);

    void updateStore(Store store);

    int countStore(String storeName, String manager);

    List<Store> findStore(int i, String storeName, String manager);

    List getAllStoreName();

    //找到门店的id
    Store findStoreId(int id);

}
