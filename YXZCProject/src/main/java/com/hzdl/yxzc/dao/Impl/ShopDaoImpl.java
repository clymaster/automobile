package com.hzdl.yxzc.dao.Impl;

import com.hzdl.yxzc.dao.ShopDao;
import com.hzdl.yxzc.domain.Store;
import com.hzdl.yxzc.util.JDBCUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * 门店模块
 */
public class ShopDaoImpl implements ShopDao {
    JdbcTemplate jdbcTemplate=new JdbcTemplate(JDBCUtils.getDataSource());
    @Override
    public List<Store> findAllStore(int i) {
        String sql="SELECT * FROM store LIMIT ?,5";
        List<Store> storeList = jdbcTemplate.query( sql, new BeanPropertyRowMapper<>( Store.class ),(i-1)*5);
        return storeList;
    }

    @Override
    public void addStore(Store store) {
        String sql="insert into store values (null ,?,?,?,?)";
        jdbcTemplate.update(sql,store.getStoreName(),store.getAddress(),store.getPhone(),store.getManager());
    }

    @Override
    public void deleteStore(String id) {
        String sql="delete from store where id=?";
        jdbcTemplate.update(sql,Integer.parseInt(id));
    }

    @Override
    public void updateStore(Store store) {
        String sql="update store set storeName=?,address=?,phone=?,manager=? where id=?";
        jdbcTemplate.update(sql,store.getStoreName(),store.getAddress(),store.getPhone(),store.getManager(),store.getId());
    }

    @Override
    public int countStore(String storeName,String manager) {
        //String sql="select count(*) from store where 1=1 and storeName like '%%' and  manager like '%%'";
        StringBuilder sb= new StringBuilder("select count(*) from store where 1=1");
        if (storeName!="" && storeName!=null){
            sb.append(" and storeName like '%"+storeName+"%'");
        }
        if (manager!="" && manager!=null){
            sb.append(" and manager like '%"+manager+"%'");
        }
        String sql = sb.toString();
        int count = jdbcTemplate.queryForObject(sql,Integer.class);
        return count;
    }

    @Override
    public List<Store> findStore(int i, String storeName, String manager) {
        //String sql="select * from store where 1=1 and manager LIKE '%'?'%' AND storeName LIKE '%'?'%' limit ?,5";
        StringBuilder sb= new StringBuilder("select * from store where 1=1");
        if (storeName!="" && storeName!=null){
            sb.append(" and storeName like '%"+storeName+"%'");
        }
        if (manager!="" && manager!=null){
            sb.append(" and manager like '%"+manager+"%'");
        }
        sb.append(" limit "+(i-1)*5+",5");
        String sql = sb.toString();
        //select * from store where 1=1 and manager LIKE '%2%' AND storeName LIKE '%北%'
        List<Store> storeList = jdbcTemplate.query( sql, new BeanPropertyRowMapper<>( Store.class ));
        return storeList;
    }

    @Override
    public List getAllStoreName() {
        String sql="select storeName from store";
       // List<Map<String, Object>> storeList = jdbcTemplate.queryForList(sql);
        List list = jdbcTemplate.queryForList(sql);
        return list;
    }

    @Override
    public Store findStoreId(int id) {
        String sql = "select * from store where id = ?";
        Store store = null;
        try {
            store = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Store.class), id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return store;
    }
}
