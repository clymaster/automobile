package com.hzdl.yxzc.controller.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hzdl.yxzc.service.*;
import com.hzdl.yxzc.service.Impl.*;
import com.hzdl.yxzc.util.IMGUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class BaseServlet extends HttpServlet {
    IMGUtils imgUtils=new IMGUtils();

    ObjectMapper objectMapper=new ObjectMapper();
    EmpService empService=new EmpServiceImpl();
    IndentService indentService=new IndentServiceImpl();
    LoginService loginService=new LoginServiceImpl();
    MemberService memberService=new MemberServiceImpl();
    ShopService shopService=new ShopServiceImpl();
    VehicleService vehicleService=new VehicleServiceImpl();
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestURI = req.getRequestURI();
        String substring = requestURI.substring(requestURI.lastIndexOf("/")+1);
        try {
            Method method = this.getClass().getMethod(substring, HttpServletRequest.class, HttpServletResponse.class);
            method.invoke(this,req,resp);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
