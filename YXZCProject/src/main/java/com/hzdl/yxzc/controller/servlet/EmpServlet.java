package com.hzdl.yxzc.controller.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hzdl.yxzc.domain.Emp;
import com.hzdl.yxzc.domain.PageInfo;
import com.hzdl.yxzc.service.EmpService;
import com.hzdl.yxzc.service.Impl.EmpServiceImpl;
import com.hzdl.yxzc.util.DesensitizedUtils;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * 员工模块
 */
@WebServlet("/empServlet/*")
public class EmpServlet extends BaseServlet{
    EmpService empService = new EmpServiceImpl();
    ObjectMapper objectMapper = new ObjectMapper();
    //注册的方法
    public void register(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取前段发送过来的数据
       // Map<String, String[]> map = req.getParameterMap();
        Emp emp = new Emp();
        //获取前段发送过来的数据
        String empName = req.getParameter("empName");
        String sex = req.getParameter("sex");
        String birthday = req.getParameter("birthday");
        String joinTime = req.getParameter("joinTime");
        String idcard = req.getParameter("idcard");
        String phone = req.getParameter("phone");
        String storeId = req.getParameter("storeId");
        if ("北京五道口店".equals(storeId)){
            emp.setStoreId(1);
        } else if ("新东站店".equals(storeId)){
            emp.setStoreId(2);
        }else if ("北京国贸店".equals(storeId)){
            emp.setStoreId(3);
        }else {
            emp.setStoreId(4);
        }
        String status = req.getParameter("status");

        if ("1".equals(status)){
            emp.setStatus(1);
        }else {
            emp.setStatus(2);
        }

        String password = req.getParameter("password");

        emp.setEmpName(empName);
        emp.setSex(sex);
        emp.setBirthday(birthday);
        emp.setJoinTime(joinTime);
        emp.setIdcard(idcard);
        emp.setPhone(phone);
        emp.setPassword(password);


//        try {
//            BeanUtils.populate(emp,map);
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
        boolean flag = empService.save(emp);
        if (flag==false){
            //用户名已存在，注册失败
            objectMapper.writeValue(resp.getWriter(),flag);
            return;
        }
        objectMapper.writeValue(resp.getWriter(),flag);
    }

    //通过获取输入框的内容（name）进行模糊查询，并通过获取当前页面以及每页的行数进行分页展示
    public void queryByName(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String currentpage = req.getParameter("currentpage");
        String rows = req.getParameter("rows");
       // String id = req.getParameter("id");
        PageInfo pageInfo = empService.queryByName(name,currentpage,rows);
        String json = DesensitizedUtils.getJson(pageInfo);
        //objectMapper.writeValue(resp.getWriter(),json);
        resp.getWriter().write(json);
//        System.out.println(id);
//        if (null != id && ""!=id){
//            System.out.println(111);
//            empService.delectById(id);
//        }
    }

    //通过id进行删除操作
    public void delectById(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        //获取当前页数，删除成功后展示删除信息所在的页面
        String currentpage = req.getParameter("currentpage");

        //String totalpage = empService.queryById(id);

        empService.delectById(id);

        objectMapper.writeValue(resp.getWriter(),currentpage);
    }

    //修改用户信息
    public void modify(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Emp emp = new Emp();
        String id = req.getParameter("id");
        String empName = req.getParameter("empName");
        String sex = req.getParameter("sex");
        String birthday = req.getParameter("birthday");
        String joinTime = req.getParameter("joinTime");
        String idcard = req.getParameter("idcard");
        String phone = req.getParameter("phone");
        String storeId = req.getParameter("storeId");
        if ("北京五道口店".equals(storeId)){
            emp.setStoreId(1);
        } else if ("新东站店".equals(storeId)){
            emp.setStoreId(2);
        }else if ("北京国贸店".equals(storeId)){
            emp.setStoreId(3);
        }else {
            emp.setStoreId(4);
        }

        emp.setId(Integer.parseInt(id));
        emp.setEmpName(empName);
        emp.setSex(sex);
        emp.setBirthday(birthday);
        emp.setJoinTime(joinTime);
        emp.setIdcard(idcard);
        emp.setPhone(phone);

        boolean flag = empService.modify(emp);
        objectMapper.writeValue(resp.getWriter(),flag);
    }


    //拿到前端的id，再点击修改按钮进入修改页面的时候通过id获取对应的数据动态展示
    public void queryById(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        List list = empService.queryById(id);
        objectMapper.writeValue(resp.getWriter(),list);
}
//   public void findEmpName(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//      String id = req.getParameter("id");
//      String empName = empService.findEmpName(id);
//  }
}
