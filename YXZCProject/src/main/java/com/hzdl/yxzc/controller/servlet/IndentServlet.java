package com.hzdl.yxzc.controller.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hzdl.yxzc.domain.DingDan;
import com.hzdl.yxzc.domain.PageInfo;
import com.hzdl.yxzc.domain.ResultInfo;
import com.hzdl.yxzc.service.Impl.IndentServiceImpl;
import com.hzdl.yxzc.service.IndentService;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 订单模块
 */
@WebServlet("/indentServlet/*")
public class IndentServlet extends BaseServlet {

    IndentService indentService = new IndentServiceImpl();

    //用来存放json
    //java对象序列化为json
    ResultInfo resultInfo = new ResultInfo();
    ObjectMapper objectMapper = new ObjectMapper();

    //创建订单
    public void creatOrder(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, ParseException {
       //1.获取订单数据
        Map<String, String[]> parameterMap = req.getParameterMap();
        //2.封装对象
        DingDan dingDan = new DingDan();
        try {
            BeanUtils.populate(dingDan, parameterMap);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //3.调用service方法完成订单生成
       /* boolean flag = indentService.createOrder(dingDan);
        if(flag){
            resultInfo.setFlag(true);
        }else {
            resultInfo.setFlag(false);
        }
        objectMapper.writeValue(resp.getWriter(), resp);*/
    }


    //订单详情
    public  void messOrder(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //根据订单编号查看订单详情
        //1.从客户端获取参数
        String id = req.getParameter("id");
        //2.调用service方法查询订单详情
        DingDan dingDan = indentService.messOrder(id);
        //3.将dingDan对象序列化为json
        objectMapper.writeValue(resp.getWriter(), dingDan);
    }

    //续租车辆订单
    public void goonOrder(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //根据订单编号查看订单详情
        //1.从客户端获取参数
        String id = req.getParameter("id");
        //2.调用service方法查询订单详情
        DingDan dingDan = indentService.goonOrder(id);
        //3.将dingDan对象序列化为json
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(), dingDan);

    }

    //续租车辆订单修改还车时间
    public void goonOrderEndTime(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, ParseException {
        //根据订单编号查看订单详情
        //DingDan dingDan = new DingDan();
        //1.从客户端获取参数
        String id = req.getParameter("id");
        String endTime = req.getParameter("endTime");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parse = simpleDateFormat.parse(endTime);
        //2.调用service方法查询订单详情
        System.out.println(id);
        System.out.println(parse);
        System.out.println(endTime);
        Boolean flag = indentService.goonOrderEndTime(id,endTime);
       // dingDan.setEndTime(endTime);

        //3.将dingDan对象序列化为json
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(), flag);

    }



    //归还车辆
    public void returnCar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //根据订单编号查看订单详情
        //1.从客户端获取参数
        String id = req.getParameter("id");
        //2.调用service方法查询订单详情
        DingDan dingDan = indentService.returnCar(id);
        //3.将dingDan对象序列化为json
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(), dingDan);
    }

    //归还车辆修改归还门店
    public void returnStore(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //根据订单编号查看订单详情
        //1.从客户端获取参数
        String id = req.getParameter("id");
        String returnStore = req.getParameter("returnStore");
        //2.调用service方法查询订单详情
        boolean flag = indentService.returnStore(id,returnStore);
        //3.将dingDan对象序列化为json
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(), flag);
    }

    //在租订单分页和模糊查询
    public  void payPageQuery(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.接收客户端传来的参数
        String rows = req.getParameter("rows");
        String currentPage = req.getParameter("currentPage");
        String cusName = req.getParameter("cusName");
        String phone = req.getParameter("phone");
        String startTime = req.getParameter("startTime");
        String endTime = req.getParameter("endTime");

        //2.调用service方法查询pageInfo对象
        PageInfo pageInfo = indentService.payPageQuery(rows,currentPage,cusName,phone,startTime,endTime);

        //3.将pageInfo对象序列化为json对象
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(), pageInfo);


    }

    //已完成订单分页和模糊查询
    public  void successPageQuery(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.接收客户端传来的参数
        String rows = req.getParameter("rows");
        String currentPage = req.getParameter("currentPage");
        String cusName = req.getParameter("cusName");
        String phone = req.getParameter("phone");
        String startTime = req.getParameter("startTime");
        String endTime = req.getParameter("endTime");

        //2.调用service方法查询pageInfo对象
        PageInfo pageInfo = indentService.successPageQuery(rows,currentPage,cusName,phone,startTime,endTime);

        //3.将pageInfo对象序列化为json对象
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(), pageInfo);

    }



}
