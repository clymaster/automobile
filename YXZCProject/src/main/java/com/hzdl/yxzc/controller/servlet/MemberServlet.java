package com.hzdl.yxzc.controller.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hzdl.yxzc.domain.*;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 会员servlet
 */
@WebServlet("/memberServlet/*")
public class MemberServlet extends BaseServlet {
    public void addVip(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Customer customer=new Customer();
        Map<String, String[]> parameterMap = req.getParameterMap();

        try {
            //解决BeanUtils不能封装Date类型
            ConvertUtils.register(new Converter() {

                @Override
                public Object convert(Class clazz, Object value) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    Date parse = null ;
                    try {
                        if(value.equals("")||value==null) {
                            value="2018-05-25";
                        }
                        parse = format.parse(value.toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return parse;
                }

            }, Date.class);
            BeanUtils.populate(customer, parameterMap);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        boolean flag=memberService.saveadd(customer);
        ResultInfo resultInfo=new ResultInfo();
        if (flag){
            resultInfo.setErrorMsg("添加成功");
            resultInfo.setFlag(flag);
        }else {
            resultInfo.setErrorMsg("添加失败");
            resultInfo.setFlag(flag);
        }
        ObjectMapper objectMapper=new ObjectMapper();
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(),resultInfo);
    }

    public void findCustomerByPhone(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String phone = req.getParameter( "phone" );
        List<Customer> customerList=memberService.findCustomerByPhone(phone);
        ResultInfo resultInfo=new ResultInfo();
        if (customerList.size()<1){
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("该会员不存在");
        }else{
            resultInfo.setFlag(true);
        }
        ObjectMapper objectMapper=new ObjectMapper();
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(),resultInfo);
    }
    public void findAllBrand(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
            List<Brand> brandList=memberService.findAllBrand();
            ObjectMapper objectMapper=new ObjectMapper();
        //String s = objectMapper.writeValueAsString( brandList );
        //System.out.println(s);
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(),brandList);
    }

    public void findAllEmp(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        List<Emp> empList=memberService.findAllEmp();
        ObjectMapper objectMapper=new ObjectMapper();
//        String s = objectMapper.writeValueAsString( empList );
//        System.out.println(s);
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(),empList);
    }

    public void findStoreBybrandName(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String brandname = req.getParameter( "brandName" );
        List<List<Store>> storeList=memberService.findStoreBybrandName(brandname);
        ObjectMapper objectMapper=new ObjectMapper();
//        String s = objectMapper.writeValueAsString( storeList );
//        System.out.println(s);
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(),storeList);
    }

    public void addOder(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Map<String, String[]> parameterMap = req.getParameterMap();
        Order order=new Order();
        try {
            BeanUtils.populate(order,parameterMap);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        ResultInfo resultInfo=memberService.addOder(order);
        ObjectMapper objectMapper=new ObjectMapper();
        resp.setContentType("application/json;charset=utf-8");
        //String s = objectMapper.writeValueAsString( resultInfo );
        //System.out.println(s);
        objectMapper.writeValue(resp.getWriter(),resultInfo);
    }
    public void alterId(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取id
        String id = req.getParameter("id");

        //调用Service查询
        Customer customer=memberService.customerById(id);
        //将customer对象存入request
        req.setAttribute("customer",customer);

        objectMapper.writeValue(resp.getWriter(),customer);




    }


    public void FindcusByPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.获取参数e

        String currentPage = req.getParameter("currentPage");
        String rows = req.getParameter("rows");

        //2.调用service查询
//        System.out.println(currentPage);
        PageBean<Customer> pb =memberService.findcusByPage(currentPage,rows);

        //3.将PageBean存入request
        req.setAttribute("pb",pb);
        String s = objectMapper.writeValueAsString(pb);
        //System.out.println(s);
        resp.setContentType("application/json;charset=utf-8");
        objectMapper.writeValue(resp.getWriter(),pb);


    }
    public void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //1.获取数据
        String cusName = req.getParameter("cusName");
        String sex = req.getParameter("sex");

        String birthday = req.getParameter("birthday");
        String adddress = req.getParameter("adddress");
        String phone = req.getParameter("phone");
        Customer customer=new Customer();
        customer.setCusName(cusName);
        customer.setSex(sex);
        Date date = new Date(birthday);
        customer.setBirthday(date);
        customer.setAdddress(adddress);
        customer.setPhone(phone);
        //调用service查询
        memberService.updatevip(customer);

        objectMapper.writeValue(resp.getWriter(),customer);

    }
}
