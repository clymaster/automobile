package com.hzdl.yxzc.controller.servlet;

import com.hzdl.yxzc.domain.Car;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.List;

@WebServlet("/POI/*")
public class DownloadServelet extends BaseServlet {
    public void exportExcel(HttpServletRequest req, HttpServletResponse response) throws Exception {

        String[] tableHeaders = {"车牌号", "乘坐人数", "排量"};

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Sheet1");
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        Font font = workbook.createFont();
//        font.setColor(HSSFColor.RED.index);
        font.setBold(true);
        cellStyle.setFont(font);

        // 将第一行的三个单元格给合并
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 2));
        HSSFRow row = sheet.createRow(0);
        HSSFCell beginCell = row.createCell(0);
        beginCell.setCellValue("通讯录");
        beginCell.setCellStyle(cellStyle);

        row = sheet.createRow(1);
        // 创建表头
        for (int i = 0; i < tableHeaders.length; i++) {
            HSSFCell cell = row.createCell(i);
            cell.setCellValue(tableHeaders[i]);
            cell.setCellStyle(cellStyle);
        }


        List<Car> cars = vehicleService.downloadFindAllCarBadService();

        for (int i = 0; i < cars.size(); i++) {
            row = sheet.createRow(i + 2);

            Car car = cars.get(i);
            row.createCell(0).setCellValue(car.getCard());
            row.createCell(1).setCellValue(car.getPeople());
            row.createCell(2).setCellValue(car.getOutput());
        }

        // 获取桌面路径
//        FileSystemView fsv = FileSystemView.getFileSystemView();
//        String desktop = fsv.getHomeDirectory().getPath();
//        String filePath = desktop + "/template.xls";
//
//        File file = new File(filePath);
//        OutputStream outputStream = new FileOutputStream(file);

        System.out.println(workbook);
        OutputStream outputStream = response.getOutputStream();
        response.reset();
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        response.setHeader("Content-disposition", "attachment;filename=template.xls");

        workbook.write(outputStream);
        outputStream.flush();
        outputStream.close();

    }

}
