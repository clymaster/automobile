package com.hzdl.yxzc.controller.servlet;

import com.hzdl.yxzc.domain.*;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.fileupload.FileUploadException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;


/**
 * 车辆模块
 */
@WebServlet("/vehicleSeverlet/*")
public class VehicleSeverlet extends BaseServlet {
    //品牌添加
    public void addBrand(HttpServletRequest req, HttpServletResponse resp) throws FileUploadException, IOException {
        String brandname = imgUtils.imgUpdate(req);
        boolean b = vehicleService.addBrandService(brandname);
        resp.sendRedirect("/YXZCProject/brand.html");
    }

    //检索所有品牌
    public void findAllBrand(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int currentpage = Integer.parseInt(req.getParameter("currentpage"));
        int pagesize = 6;
        PageInfo brands = vehicleService.findAllBrandService(currentpage, pagesize);
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setData(brands);
        objectMapper.writeValue(resp.getWriter(), resultInfo);
    }

    //修改车辆品牌
    public void updateBrand(HttpServletRequest req, HttpServletResponse resp) throws IOException, FileUploadException {
        Cookie[] cookies = req.getCookies();
        String brandname = (String) req.getSession().getAttribute("brandname");
        String newBrandName = imgUtils.imgUpdate(req);
        boolean flag = vehicleService.updateBrandService(newBrandName, brandname);
        System.out.println(flag);
        resp.sendRedirect("/YXZCProject/brand.html");
    }

    //保存当前选中的车辆品牌到session
    public void saveSession(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String brandname = req.getParameter("brandname");
        String decode = URLDecoder.decode(brandname);
        req.getSession().setAttribute("brandname", decode);
        resp.getWriter();
    }

    //删除品牌信息
    public void deleteBrand(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String brandnameISO = req.getParameter("brandname");
        byte[] b = brandnameISO.getBytes("ISO-8859-1");
        String brandname = new String(b, "utf-8");
        System.out.println(brandname);
        boolean flag = vehicleService.deleteBrandByNameService(brandname);
        resp.sendRedirect("/YXZCProject/brand.html");
    }

    //查找品牌名字
    public void findBrandName(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<String> brandnames = vehicleService.findBrandNameService();
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setData(brandnames);
        objectMapper.writeValue(resp.getWriter(), resultInfo);
    }

    //添加车辆
    public void addCar(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String brand_select = req.getParameter("brandname");//品牌
        int brandid = vehicleService.findBrandIdService(brand_select);
        String type = req.getParameter("type");//车辆类型
        String bsx = req.getParameter("bsx");//操作类型
        String people = req.getParameter("people");//乘客容量
        String card = req.getParameter("card");//车牌

        String style = req.getParameter("style");//车辆型号
        String output = req.getParameter("output");//排量
        String price = req.getParameter("price");//价格
        String storeId = req.getParameter("storeId");//门店
        Car car = new Car(brandid, type, style, bsx, output, people, price, card, "2");
        boolean flag = vehicleService.addCarService(car);
        resp.sendRedirect("/YXZCProject/find_car.html");
    }

    //查询车辆
    public void findcar(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int currentpage = Integer.parseInt(req.getParameter("currentpage"));
        String brandnameISO = req.getParameter("brandname");
        byte[] bytes = brandnameISO.getBytes("ISO-8859-1");
        String brandname=new String(bytes,"utf-8");
        String people = req.getParameter("people");
        int pagesize = 6;
        PageInfo<Car> pageInfos = vehicleService.findAllCarService(currentpage, pagesize, brandname, people);
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setData(pageInfos);
        objectMapper.writeValue(resp.getWriter(), resultInfo);
    }

    //删除车辆
    public void deletecar(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String id = req.getParameter("id");
        System.out.println(id);
        boolean flag = vehicleService.deleteCarByIdService(id);
        ResultInfo resultInfo = new ResultInfo();
        if (flag) {
            resp.sendRedirect("/YXZCProject/find_car.html");
        } else {
            resultInfo.setFlag(false);
            resultInfo.setErrorMsg("删除失败");
            objectMapper.writeValue(resp.getWriter(), resultInfo);
        }
    }

    //修改车辆
    public void updateCar(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String carid = req.getParameter("carid");//车辆id
        String brand_select = req.getParameter("brandname");//品牌
        int brandid = vehicleService.findBrandIdService(brand_select);
        String type = req.getParameter("type");//车辆类型
        String bsx = req.getParameter("bsx");//操作类型
        String people = req.getParameter("people");//乘客容量
        String card = req.getParameter("card");//车牌
        String style = req.getParameter("style");//车辆型号
        String output = req.getParameter("output");//排量
        String price = req.getParameter("price");//价格
        String storeId = req.getParameter("storeId");//门店
        Car car = new Car(brandid, type, style, bsx, output, people, price, card, "2");
        System.out.println(car);
        boolean flag = vehicleService.updateCarService(carid, car);
        ResultInfo resultInfo = new ResultInfo();
        objectMapper.writeValue(resp.getWriter(), resultInfo);

    }

    //查询报废车辆
    public void findcarbad(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int currentpage = Integer.parseInt(req.getParameter("currentpage"));
        String brandname = req.getParameter("brandname");
//        byte[] bytes = brandnameISO.getBytes("ISO-8859-1");
//        String brandname=new String(bytes,"utf-8");
        String people = req.getParameter("people");
        int pagesize = 6;
        PageInfo<Car> pageInfos = vehicleService.findAllCarBadService(currentpage, pagesize, brandname, people);
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setData(pageInfos);
        objectMapper.writeValue(resp.getWriter(), resultInfo);
    }

    //查看报废信息
    public void SeeCarBadMes(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Integer carId = Integer.parseInt(req.getParameter("carId"));
        CarBad carBad = vehicleService.SeeCarBadMesService(carId);
        objectMapper.writeValue(resp.getWriter(), carBad);

    }

    //汽车报废
    public void carScrap(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //损坏部位复选框是复选框得来的参数为数组，用下方法获取
        String[] check_values = req.getParameterValues("check_values");
        StringBuilder badPart_builder = new StringBuilder();
        for (String check_value : check_values) {
            badPart_builder = badPart_builder.append(check_value + " ");
        }
        String badPart = badPart_builder.toString();
        String card = req.getParameter("card");
        String describe = req.getParameter("describe");

        boolean flag = vehicleService.badCarService(badPart, card, describe);
        objectMapper.writeValue(resp.getWriter(), flag);

    }

    //展示品牌下拉框
    public void displayBrandOption(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<Brand> list_brand = vehicleService.findAllBrand();
        objectMapper.writeValue(resp.getWriter(), list_brand);

    }

    //展示门店下拉框
    public void displayStoreOption(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<Store> list_store = vehicleService.findAllStore();
        objectMapper.writeValue(resp.getWriter(), list_store);

    }

    //点修改的时候展示原车的信息
    public void seeUpdateBadCar(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String carId = req.getParameter("carId");
        Car car = vehicleService.modifyBadCarService(Integer.parseInt(carId));
        objectMapper.writeValue(resp.getWriter(), car);
    }

    //修改原车信息
    public void UpdateBadCar(HttpServletRequest req, HttpServletResponse resp) throws IOException, InvocationTargetException, IllegalAccessException {
        Map<String, String[]> parameterMap = req.getParameterMap();

        String brandName = parameterMap.get("brandname")[0];
        String storeName = parameterMap.get("storeName")[0];

        int brandId = vehicleService.findOneBrandIdByBrandName(brandName);
        int storeId = vehicleService.findOneStoreIdByBrandName(storeName);
        Car car = new Car();
        BeanUtils.populate(car, parameterMap);
        car.setBrandid(brandId);
        car.setStoreId(storeId+"");
        boolean flag = vehicleService.realModify(car);
        objectMapper.writeValue(resp.getWriter(), flag);
    }

    //删除报损汽车
    public void deleteBadCar(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String carId = req.getParameter("carId");

        //先从应该从carbad表中删除对应车损信息
        vehicleService.deleteBadCarMesSercice(Integer.parseInt(carId));

        //再从car表中删除该报损车
        vehicleService.deleteBadCarSercice(Integer.parseInt(carId));


        objectMapper.writeValue(resp.getWriter(),true);


    }

    //修复报损汽车
    public void mendBadCar(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String carId = req.getParameter("carId");

        //先从应该从carbad表中删除对应车损信息
        vehicleService.deleteBadCarMesSercice(Integer.parseInt(carId));

        //再从car表中修复该报损车
        vehicleService.mendBadCarSercice(Integer.parseInt(carId));


        objectMapper.writeValue(resp.getWriter(),true);


    }

}
