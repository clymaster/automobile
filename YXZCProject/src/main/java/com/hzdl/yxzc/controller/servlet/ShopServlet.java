package com.hzdl.yxzc.controller.servlet;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hzdl.yxzc.domain.PageBean;
import com.hzdl.yxzc.domain.ResultInfo;
import com.hzdl.yxzc.domain.Store;
import com.hzdl.yxzc.service.Impl.ShopServiceImpl;
import com.hzdl.yxzc.service.ShopService;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

/**
 * 门店模块
 */
@WebServlet("/shopServlet/*")
public class ShopServlet extends BaseServlet {
    ShopService shopService=new ShopServiceImpl();

    public void findAllStore (HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setCharacterEncoding( "utf-8");
        String currentPage = req.getParameter( "currentPage" );
        String storeName = URLDecoder.decode(req.getParameter( "storeName" ),"utf-8" );
        String manager = URLDecoder.decode(req.getParameter( "manager" ),"utf-8" );
        ObjectMapper objectMapper=new ObjectMapper();
        //System.out.println(storeName+":"+manager);
        PageBean<Store> pageBean=shopService.findAllStore(currentPage,storeName,manager);
        resp.setContentType("application/json;charset=utf-8");
        String s = objectMapper.writeValueAsString( pageBean );
        //System.out.println(s);
        objectMapper.writeValue(resp.getWriter(),pageBean);
    }

    public void addStore(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException,IOException{
        Map<String, String[]> parameterMap = req.getParameterMap();
        Store store=new Store();
        try {
            BeanUtils.populate(store,parameterMap);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //System.out.println(store);
        shopService.addStore(store);
        ResultInfo resultInfo=new ResultInfo();
        resultInfo.setErrorMsg("添加成功！");
        resp.setContentType("application/json;charset=utf-8");
        ObjectMapper objectMapper=new ObjectMapper();
        objectMapper.writeValue(resp.getWriter(),resultInfo);
    }
    public void deleteStore(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException,IOException {
        String id = req.getParameter( "id" );
        shopService.deleteStore(id);
        resp.setContentType("application/json;charset=utf-8");
        ObjectMapper objectMapper=new ObjectMapper();
        ResultInfo resultInfo=new ResultInfo();
        objectMapper.writeValue(resp.getWriter(),resultInfo);
    }

    public void updateStore(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException,IOException {
        Map<String, String[]> parameterMap = req.getParameterMap();
        Store store=new Store();
        try {
            BeanUtils.populate(store,parameterMap);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        shopService.updateStore(store);
        ResultInfo resultInfo=new ResultInfo();
        resultInfo.setErrorMsg("修改成功！");
        resp.setContentType("application/json;charset=utf-8");
        ObjectMapper objectMapper=new ObjectMapper();
        objectMapper.writeValue(resp.getWriter(),resultInfo);
    }
    public void getAllStoreName(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException,IOException {
        List storeList= shopService.getAllStoreName();
        ObjectMapper objectMapper=new ObjectMapper();
        objectMapper.writeValue(resp.getWriter(),storeList);
    }
}
