package com.hzdl.yxzc.controller.servlet;

import com.aliyuncs.exceptions.ClientException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hzdl.yxzc.domain.Emp;
import com.hzdl.yxzc.service.Impl.LoginServiceImpl;
import com.hzdl.yxzc.service.LoginService;
import com.hzdl.yxzc.util.AliyunSmsUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 登录模块
 */
@WebServlet("/loginServlet/*")
public class LoginServelt extends BaseServlet {
    public void login(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(111111111);
        //获取参数
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        Emp emp = loginService.empLogin(username, password);
        HttpSession session = req.getSession();
        if (emp == null) {
            //登录失败
        } else {
            //登录成功
            //将登录的emp对象存入session
            session.setAttribute("emp", emp);
        }

        //将emp序列化为json对象
        objectMapper.writeValue(resp.getWriter(), emp);
    }

    //退出系统
    public void exit(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //从session中移除登录对象
        req.getSession().removeAttribute("emp");
        //自定义的一个标签用来序列化为json对象
        boolean flag = true;
        objectMapper.writeValue(resp.getWriter(), flag);

    }

    //根据是否登录 主页显示的情况
    public void isLogin(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //从session中获取登录对象
        Emp emp = (Emp) req.getSession().getAttribute("emp");

        objectMapper.writeValue(resp.getWriter(), emp);

    }

    //找回密码
    public void findPassword(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, ClientException {
        //获取参数
        String empName = req.getParameter("empName");
        String tel = req.getParameter("tel");

        //先根据参数查询是否有这个人
        Emp emp = loginService.findOnePvm(empName, tel);

        if (emp == null) {
            //没有这个人
        } else {
            //有这个人 那么就发送短信
            AliyunSmsUtils.setNewcode();
            String code = Integer.toString(AliyunSmsUtils.getNewcode());
            AliyunSmsUtils.sendSms(tel, code);
            //把验证码存入session 便于验证用户输入的
            req.getSession().setAttribute("code_produce", code);
        }

        objectMapper.writeValue(resp.getWriter(), emp);
    }

    //校验短信验证码
    public void checkCode(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, ClientException {
        //获取用户输入的短信验证码
        String code_input = req.getParameter("code");
        //从session中获取短信验证码
        String code_produce = (String) req.getSession().getAttribute("code_produce");
        boolean flag = false;
        if (code_input != null && code_input.equals(code_produce)) {
            //验证码匹配
            flag = true;
        }

        objectMapper.writeValue(resp.getWriter(), flag);

    }



}
