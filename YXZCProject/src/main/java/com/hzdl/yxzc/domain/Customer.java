package com.hzdl.yxzc.domain;

import java.util.Date;

public class Customer {
    private  int id;
    private  String cusName;//会员名
    private  String sex;//性别
    private Date birthday;//出生年月
    private  String adddress;//家庭住址
    private  String phone;//手机号
    private Double yajin;//押金金额
    private Date createTime;//创建时间

    public Customer(){};

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", cusName='" + cusName + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday=" + birthday +
                ", adddress='" + adddress + '\'' +
                ", phone='" + phone + '\'' +
                ", yajin=" + yajin +
                ", createTime=" + createTime +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAdddress() {
        return adddress;
    }

    public void setAdddress(String adddress) {
        this.adddress = adddress;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getYajin() {
        return yajin;
    }

    public void setYajin(Double yajin) {
        this.yajin = yajin;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Customer(int id, String cusName, String sex, Date birthday, String adddress, String phone, Double yajin, Date createTime) {

        this.id = id;
        this.cusName = cusName;
        this.sex = sex;
        this.birthday = birthday;
        this.adddress = adddress;
        this.phone = phone;
        this.yajin = yajin;
        this.createTime = createTime;
    }
}
