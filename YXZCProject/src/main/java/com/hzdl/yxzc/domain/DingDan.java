package com.hzdl.yxzc.domain;

import java.util.Date;

public class DingDan {
    private Integer id;
    private String orderId;	//订单编号
    private Integer customerId;	//会员ID
    private Integer carId;	//汽车ID
    private Date startTime;	//借车时间
    private Date endTime;//还车时间
    private String empName;	//操作员工姓名
    private Double total;	//总费用
    private Integer time;	//租用时长/天
    private Integer status;	//订单状态  1表示在租，0表示已完成
    private String borrowStore;	//借车门店
    private String returnStore;	//还车门店
    private Double price;	//租金单价	/
    private Integer empId;	//员工ID
    private String carMess;	//车辆信息
    private Integer storeId;	//借车门店ID
    private String cusName;	//会员名
    private String phone;	//会员手机号
    private Date createTime;	//创建时间
    private Store store;//门店
    private  Customer customer;//用户信息
    public DingDan(){}

    @Override
    public String toString() {
        return "DingDan{" +
                "id=" + id +
                ", orderId='" + orderId + '\'' +
                ", customerId=" + customerId +
                ", carId=" + carId +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", empName='" + empName + '\'' +
                ", total=" + total +
                ", time=" + time +
                ", status=" + status +
                ", borrowStore='" + borrowStore + '\'' +
                ", returnStore='" + returnStore + '\'' +
                ", price=" + price +
                ", empId=" + empId +
                ", carMess='" + carMess + '\'' +
                ", storeId=" + storeId +
                ", cusName='" + cusName + '\'' +
                ", phone='" + phone + '\'' +
                ", createTime=" + createTime +
                ", store=" + store +
                ", customer=" + customer +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBorrowStore() {
        return borrowStore;
    }

    public void setBorrowStore(String borrowStore) {
        this.borrowStore = borrowStore;
    }

    public String getReturnStore() {
        return returnStore;
    }

    public void setReturnStore(String returnStore) {
        this.returnStore = returnStore;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    public String getCarMess() {
        return carMess;
    }

    public void setCarMess(String carMess) {
        this.carMess = carMess;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public DingDan(Integer id, String orderId, Integer customerId, Integer carId, Date startTime, Date endTime, String empName, Double total, Integer time, Integer status, String borrowStore, String returnStore, Double price, Integer empId, String carMess, Integer storeId, String cusName, String phone, Date createTime, Store store, Customer customer) {

        this.id = id;
        this.orderId = orderId;
        this.customerId = customerId;
        this.carId = carId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.empName = empName;
        this.total = total;
        this.time = time;
        this.status = status;
        this.borrowStore = borrowStore;
        this.returnStore = returnStore;
        this.price = price;
        this.empId = empId;
        this.carMess = carMess;
        this.storeId = storeId;
        this.cusName = cusName;
        this.phone = phone;
        this.createTime = createTime;
        this.store = store;
        this.customer = customer;
    }
}
