package com.hzdl.yxzc.domain;

import com.hzdl.yxzc.domain.Brand;

import java.util.List;

public class PageInfo<T> {
    List<T> brands;
    int datacount;
    int pagetatol;

    public PageInfo() {
    }

    public PageInfo(List<T> brands, int datacount, int pagetatol) {
        this.brands = brands;
        this.datacount = datacount;
        this.pagetatol = pagetatol;
    }

    public int getDatacount() {
        return datacount;
    }

    public void setDatacount(int datacount) {
        this.datacount = datacount;
    }

    public List<T> getBrands() {
        return brands;
    }

    public void setBrands(List<T> brands) {
        this.brands = brands;
    }

    public int getPagetatol() {
        return pagetatol;
    }

    public void setPagetatol(int pagetatol) {
        this.pagetatol = pagetatol;
    }

    @Override
    public String toString() {
        return "PageInfo{" +
                "brands=" + brands +
                ", pagetatol=" + pagetatol +
                '}';
    }
}
