package com.hzdl.yxzc.domain;

import java.util.List;

//专门做的分页对象，  无论什么类型都可以用 ，所以加了一个泛型
public class PageBean<T> {
    private  int totalCount;    //总条数（总记录数）
    private  int totalPage;     //总页数
    private List<T> pageList; //分页查询出来的数据
    private  int currentPage;//当前页码
    private  int rows;       //每页显示的条数
    public PageBean() {
    }

    public PageBean(int totalCount, int totalPage, List<T> pageList, int currentPage, int rows) {
        this.totalCount = totalCount;
        this.totalPage = totalPage;
        this.pageList = pageList;
        this.currentPage = currentPage;
        this.rows = rows;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public List<T> getPageList() {
        return pageList;
    }

    public void setPageList(List<T> pageList) {
        this.pageList = pageList;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    @Override
    public String toString() {
        return "PageBean{" +
                "totalCount=" + totalCount +
                ", totalPage=" + totalPage +
                ", pageList=" + pageList +
                ", currentPage=" + currentPage +
                ", rows=" + rows +
                '}';
    }
}
