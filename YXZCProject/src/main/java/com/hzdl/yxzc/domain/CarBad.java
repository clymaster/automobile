package com.hzdl.yxzc.domain;

public class CarBad {
    //ID		int
    //carId	汽车ID	int
    //part	车损部位	varchar
    //mess	受损说明	varchar

    private int id;
    private  int carId;//汽车ID
    private String part;//车损部位
    private String mess;//受损说明

    public CarBad(){};
    public CarBad(int id, int carId, String part, String mess) {
        this.id = id;
        this.carId = carId;
        this.part = part;
        this.mess = mess;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getMess() {
        return mess;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    @Override
    public String toString() {
        return "CarBad{" +
                "id=" + id +
                ", carId=" + carId +
                ", part='" + part + '\'' +
                ", mess='" + mess + '\'' +
                '}';
    }
}
