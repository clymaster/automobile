package com.hzdl.yxzc.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Car {
    //ID brandId type style bsx output people price card storeId lease health createTime
    private int id;
    int brandid;//品牌id
    String type;//车辆类型
    String style;//几箱
    String bsx;//自动手动？
    String output;//排量
    String people;//容量
    String price;//价格
    String card;//车牌
    String storeId;//门店
    String brandname;//品牌名字
    String storeName;//门店名称
    String leasestutas;
    String healthstutas;
    private int lease;//租赁状态
    private int health; //车况
    private Date createTime;//创建时间

    public Car(){};

    public Car(int brandid, String type, String style, String bsx, String output, String people, String price, String card, String storeId) {
        this.brandid = brandid;
        this.type = type;
        this.style = style;
        this.bsx = bsx;
        this.output = output;
        this.people = people;
        this.price = price;
        this.card = card;
        this.storeId = storeId;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brandid=" + brandid +
                ", type='" + type + '\'' +
                ", style='" + style + '\'' +
                ", bsx='" + bsx + '\'' +
                ", output='" + output + '\'' +
                ", people='" + people + '\'' +
                ", price='" + price + '\'' +
                ", card='" + card + '\'' +
                ", storeId='" + storeId + '\'' +
                ", lease=" + lease +
                ", health=" + health +
                ", createTime=" + createTime +
                '}';
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getLeasestutas() {
        return leasestutas;
    }

    public void setLeasestutas(String leasestutas) {
        this.leasestutas = leasestutas;
    }

    public String getHealthstutas() {
        return healthstutas;
    }

    public void setHealthstutas(String healthstutas) {
        this.healthstutas = healthstutas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBrandid() {
        return brandid;
    }

    public void setBrandid(int brandid) {
        this.brandid = brandid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getBsx() {
        return bsx;
    }

    public void setBsx(String bsx) {
        this.bsx = bsx;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public int getLease() {
        return lease;
    }

    public void setLease(int lease) {
        this.lease = lease;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
