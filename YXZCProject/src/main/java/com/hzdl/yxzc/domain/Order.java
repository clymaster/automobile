package com.hzdl.yxzc.domain;

public class Order {
    //{"memberPhone":["159"],"brandName":["名爵"],
    // "endDate":["2020-11-29"],"empName":["admin"],
    // "storeName":["开封店"],"startDate":["2018-01-01"]}
    private String memberPhone;
    private String brandName;
    private String startDate;
    private String endDate;
    private String empName;
    private String storeName;
    public Order(){};

    @Override
    public String toString() {
        return "Order{" +
                "memberPhone='" + memberPhone + '\'' +
                ", brandName='" + brandName + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", empName='" + empName + '\'' +
                ", storeName='" + storeName + '\'' +
                '}';
    }

    public String getMemberPhone() {
        return memberPhone;
    }

    public void setMemberPhone(String memberPhone) {
        this.memberPhone = memberPhone;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Order(String memberPhone, String brandName, String startDate, String endDate, String empName, String storeName) {
        this.memberPhone = memberPhone;
        this.brandName = brandName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.empName = empName;
        this.storeName = storeName;
    }
}
