package com.hzdl.yxzc.domain;

import javax.sql.DataSource;
import java.util.Date;

public class Brand {

    private int id;
    private String brandname;//品牌名
    private Date createTime;//创建时间
    public Brand (){};

    @Override
    public String toString() {
        return "Brand{" +
                "id=" + id +
                ", brandname='" + brandname + '\'' +
                ", createTime=" + createTime +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Brand(int id, String brandname, Date createTime) {

        this.id = id;
        this.brandname = brandname;
        this.createTime = createTime;
    }
}
