package com.hzdl.yxzc.domain;

import com.hzdl.yxzc.util.Desensitized;
import com.hzdl.yxzc.util.SensitiveTypeEnum;

import java.util.Date;

public class Emp {
    // Id empId empName sex birthday joinTime idcard phone storeId status password
    private int id;
    private String empId;//员工编号
    private String empName;//员工姓名
    private String sex; //性别
    private String birthday;//出生年月
    private String joinTime;//入职日期
    @Desensitized(type = SensitiveTypeEnum.ID_CARD)
    private String idcard;//身份证号

    @Desensitized(type = SensitiveTypeEnum.MOBILE_PHONE)
    private String phone;//手机号
    private int storeId; //所属门店ID
    private int status;//员工角色
    private String password;//登录密码

    public Emp(){};
    public Emp(int id, String empId, String empName, String sex, String birthday, String joinTime, String idcard, String phone, int storeId, int status, String password) {
        this.id = id;
        this.empId = empId;
        this.empName = empName;
        this.sex = sex;
        this.birthday = birthday;
        this.joinTime = joinTime;
        this.idcard = idcard;
        this.phone = phone;
        this.storeId = storeId;
        this.status = status;
        this.password = password;
    };

    public Emp(String empName, String password) {
        this.empName = empName;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "id=" + id +
                ", empId='" + empId + '\'' +
                ", empName='" + empName + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday=" + birthday +
                ", joinTime=" + joinTime +
                ", idcard='" + idcard + '\'' +
                ", phone='" + phone + '\'' +
                ", storeId=" + storeId +
                ", status=" + status +
                ", password='" + password + '\'' +
                '}';
    }
}
