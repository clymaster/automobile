package com.hzdl.yxzc.domain;

public class Store {
    private int id;
    private String storeName;	//门店名称
    private String address;	//门店地址
    private String phone;	//联系电话
    private String manager;//	店长

    public Store (){};

    @Override
    public String toString() {
        return "Store{" +
                "id=" + id +
                ", storeName='" + storeName + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", manager='" + manager + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public Store(int id, String storeName, String address, String phone, String manager) {

        this.id = id;
        this.storeName = storeName;
        this.address = address;
        this.phone = phone;
        this.manager = manager;
    }
}
