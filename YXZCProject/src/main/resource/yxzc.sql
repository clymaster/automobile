/*
Navicat MySQL Data Transfer

Source Server         : tp
Source Server Version : 50550
Source Host           : localhost:3306
Source Database       : yxzc

Target Server Type    : MYSQL
Target Server Version : 50550
File Encoding         : 65001

Date: 2017-08-29 01:28:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for brand
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brandName` varchar(255) NOT NULL COMMENT '品牌名',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES ('1', '奥迪', '2017-08-27 17:06:57');
INSERT INTO `brand` VALUES ('2', '别克', '2017-08-27 17:07:25');
INSERT INTO `brand` VALUES ('3', '宝马', '2017-08-27 17:19:07');
INSERT INTO `brand` VALUES ('4', '本田', '2017-08-27 17:19:54');
INSERT INTO `brand` VALUES ('5', '丰田', '2017-08-27 17:20:00');
INSERT INTO `brand` VALUES ('6', '雪佛兰', '2017-08-27 17:20:09');
INSERT INTO `brand` VALUES ('13', '陆风', '2017-08-29 00:35:20');
INSERT INTO `brand` VALUES ('14', '名爵', '2017-08-29 00:46:23');

-- ----------------------------
-- Table structure for car
-- ----------------------------
DROP TABLE IF EXISTS `car`;
CREATE TABLE `car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brandId` int(11) DEFAULT NULL COMMENT '所属品牌ID',
  `type` varchar(255) DEFAULT NULL COMMENT '型号',
  `style` varchar(255) DEFAULT NULL COMMENT '款式',
  `bsx` varchar(255) DEFAULT NULL COMMENT '变速箱',
  `output` varchar(255) DEFAULT NULL COMMENT '排量',
  `people` varchar(255) DEFAULT NULL COMMENT '乘客数',
  `price` varchar(255) DEFAULT NULL COMMENT '租金',
  `card` varchar(255) DEFAULT NULL COMMENT '车牌号',
  `storeId` int(11) DEFAULT NULL COMMENT '所属门店ID',
  `lease` int(11) DEFAULT NULL COMMENT '租赁状态，0入库，1出租',
  `health` int(11) DEFAULT NULL COMMENT '车况，0正常，1车损',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `car_ibfk_1` (`brandId`),
  KEY `car_ibfk_2` (`storeId`),
  CONSTRAINT `car_ibfk_1` FOREIGN KEY (`brandId`) REFERENCES `brand` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `car_ibfk_2` FOREIGN KEY (`storeId`) REFERENCES `store` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of car
-- ----------------------------
INSERT INTO `car` VALUES ('1', '3', '350i', '三厢', '自动', '2.0T', '5', '1990', '京A-00001', '3', '0', '1', '2017-08-27 18:50:02');
INSERT INTO `car` VALUES ('2', '1', 'A4L', '三厢', '自动', '2.0T', '5', '280', '豫B-521521', '2', '0', '0', '2017-08-27 21:18:41');
INSERT INTO `car` VALUES ('3', '14', 'MG5', '两厢', '手动', '1.6L', '5', '100', '豫B-py499', '4', '0', '0', '2017-08-29 00:47:08');

-- ----------------------------
-- Table structure for carbad
-- ----------------------------
DROP TABLE IF EXISTS `carbad`;
CREATE TABLE `carbad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carId` int(11) DEFAULT NULL COMMENT '汽车ID',
  `part` varchar(255) DEFAULT NULL COMMENT '车损部位',
  `mess` varchar(255) DEFAULT NULL COMMENT '受损说明',
  PRIMARY KEY (`id`),
  KEY `carbad_ibfk_1` (`carId`),
  CONSTRAINT `carbad_ibfk_1` FOREIGN KEY (`carId`) REFERENCES `car` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of carbad
-- ----------------------------
INSERT INTO `carbad` VALUES ('7', '1', '[其他]', '');

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cusName` varchar(255) NOT NULL COMMENT '会员名',
  `sex` varchar(4) NOT NULL COMMENT '性别',
  `birthday` date NOT NULL COMMENT '出生年月',
  `address` varchar(255) NOT NULL COMMENT '家庭住址',
  `phone` varchar(255) NOT NULL COMMENT '手机号',
  `yajin` double NOT NULL DEFAULT '0' COMMENT '押金',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES ('13', '丽丽', '女', '2017-08-28', '郑州', '159', '0', '2017-08-28 22:49:37');
INSERT INTO `customer` VALUES ('14', '娜娜', '女', '2010-01-01', '河南郑州', '13938431224', '0', '2017-08-29 00:52:02');

-- ----------------------------
-- Table structure for dingdan
-- ----------------------------
DROP TABLE IF EXISTS `dingdan`;
CREATE TABLE `dingdan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) DEFAULT NULL COMMENT '会员ID',
  `carId` int(11) DEFAULT NULL COMMENT '汽车ID',
  `startTime` datetime DEFAULT NULL COMMENT '借车时间',
  `endTime` datetime DEFAULT NULL COMMENT '还车时间',
  `empName` varchar(255) DEFAULT NULL COMMENT '操作员工姓名',
  `total` double DEFAULT NULL COMMENT '总计费用',
  `orderId` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `time` int(11) DEFAULT NULL COMMENT '用车时长（天）',
  `status` int(11) DEFAULT NULL COMMENT '订单状态，0归还，1在租',
  `borrowStore` varchar(255) DEFAULT NULL COMMENT '取车门店',
  `returnStore` varchar(255) DEFAULT NULL COMMENT '还车门店',
  `price` double DEFAULT NULL COMMENT '租借车辆单价',
  `empId` varchar(255) DEFAULT NULL COMMENT '操作员工编号',
  `carMess` varchar(255) DEFAULT NULL COMMENT '汽车信息',
  `storeId` int(11) DEFAULT NULL COMMENT '门店ID',
  `createTime` datetime DEFAULT NULL,
  `cusName` varchar(255) DEFAULT NULL COMMENT '会员名',
  `phone` varchar(255) DEFAULT NULL COMMENT '会员电话',
  PRIMARY KEY (`id`),
  KEY `order_ibfk_1` (`customerId`) USING BTREE,
  KEY `order_ibfk_2` (`carId`) USING BTREE,
  KEY `order_ibfk_3` (`empName`) USING BTREE,
  KEY `storeId` (`storeId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dingdan
-- ----------------------------
INSERT INTO `dingdan` VALUES ('29', '14', '3', '2017-08-28 00:12:00', '2017-10-27 00:12:00', '小明', '6100', 'c-20170829125338473', '4', '0', '开封店', '新东站店', '100', '172934282', '名爵MG5 | 两厢1.6L手动5座', '4', '2017-08-29 00:53:38', '娜娜', '13938431224');
INSERT INTO `dingdan` VALUES ('30', '13', '2', '2017-08-28 00:12:00', '2017-08-30 00:12:00', '小明', '840', 'c-20170829011558629', '3', '1', '新东站店', 'null', '280', '172934282', '奥迪A4L | 三厢2.0T自动5座', '2', '2017-08-29 01:15:58', '丽丽', '159');

-- ----------------------------
-- Table structure for emp
-- ----------------------------
DROP TABLE IF EXISTS `emp`;
CREATE TABLE `emp` (
  `empId` varchar(255) NOT NULL COMMENT '员工编号',
  `empName` varchar(255) DEFAULT NULL COMMENT '员工姓名',
  `sex` varchar(4) DEFAULT NULL COMMENT '性别',
  `birthday` date DEFAULT NULL COMMENT '出生年月',
  `joinTime` date DEFAULT NULL COMMENT '入职日期',
  `idcard` varchar(255) DEFAULT NULL COMMENT '身份证',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机号',
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `storeId` int(11) DEFAULT NULL COMMENT '所属门店',
  `password` varchar(255) DEFAULT NULL COMMENT '登录密码',
  `status` int(11) DEFAULT NULL COMMENT '角色，0为店员，1为主管',
  PRIMARY KEY (`id`),
  KEY `emp_ibfk_1` (`storeId`),
  CONSTRAINT `emp_ibfk_1` FOREIGN KEY (`storeId`) REFERENCES `store` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of emp
-- ----------------------------
INSERT INTO `emp` VALUES ('17264396', 'admin', '男', '2000-02-01', '2017-08-26', '410221199102131010', '13098908909', '8', '3', '21232f297a57a5a743894a0e4a801fc3', '9');
INSERT INTO `emp` VALUES ('172711774', '张三', '女', '2017-08-27', '2017-08-27', '1234567890', '11111', '29', '1', 'b59c67bf196a4758191e42f76670ceba', '0');
INSERT INTO `emp` VALUES ('172728265', '韩梅梅', '女', '2019-11-28', '2017-08-27', '22222', '111111', '30', '2', '81dc9bdb52d04dc20036dbd8313ed055', '1');
INSERT INTO `emp` VALUES ('172934282', '小明', '女', '2009-01-01', '2017-08-29', '41010101010', '1399990000', '31', '3', '81dc9bdb52d04dc20036dbd8313ed055', '1');
INSERT INTO `emp` VALUES ('172922493', '小王', '男', '2017-08-29', '2017-08-29', '11111', '22222', '32', '1', '81dc9bdb52d04dc20036dbd8313ed055', '0');

-- ----------------------------
-- Table structure for store
-- ----------------------------
DROP TABLE IF EXISTS `store`;
CREATE TABLE `store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storeName` varchar(255) DEFAULT NULL COMMENT '门店名称',
  `address` varchar(255) DEFAULT NULL COMMENT '门店地址',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `manager` varchar(255) DEFAULT NULL COMMENT '店长，员工编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of store
-- ----------------------------
INSERT INTO `store` VALUES ('1', '北京五道口店', '海淀区成府路27号花园美食城2层', '18910887540', '172621334');
INSERT INTO `store` VALUES ('2', '新东站店', '东风南路高铁站', '037109876544', '12345');
INSERT INTO `store` VALUES ('3', '北京国贸店', '国贸新领地110号', '13098776567', '172643880');
INSERT INTO `store` VALUES ('4', '开封店', '大梁路', '120000', '小明');
